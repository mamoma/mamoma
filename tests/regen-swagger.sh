#!/bin/sh

set -e
set -x

abs_self="$(readlink -f -- "$0")"
tests_dir="$(dirname -- "$abs_self")"
tests_dirname="$(basename -- "$tests_dir")"
[ "$tests_dirname" = 'tests' ]

mamoma_dir="$(dirname -- "$tests_dir")"
config_subdir="$tests_dirname/config/clients/swagger"
swagger_subdir="$tests_dirname/python/clients/swagger"
[ -f "$mamoma_dir/openapi.json" ]
[ -f "$mamoma_dir/$config_subdir/config.json" ]

rm -rf -- "$mamoma_dir/$swagger_subdir/mamoma_client"

docker run --rm -it -u "$(id -u):$(id -g)" -v "$mamoma_dir:/opt/mamoma" -- \
	swaggerapi/swagger-codegen-cli-v3:3.0.46 generate \
	-l python \
	-o "/opt/mamoma/$swagger_subdir/mamoma_client" \
	-i /opt/mamoma/openapi.json \
	-c "/opt/mamoma/$config_subdir/config.json"
