#!/bin/sh

set -e
set -x

if [ ! -f "$OPC_VENV/bin/openapi-python-client" ]; then
	echo 'Please point OPC_VENV to a prepared virtual environment' 1>&2
	exit 1
fi
export PATH="$OPC_VENV/bin:$PATH"

abs_self="$(readlink -f -- "$0")"
tests_dir="$(dirname -- "$abs_self")"
tests_dirname="$(basename -- "$tests_dir")"
[ "$tests_dirname" = 'tests' ]

mamoma_dir="$(dirname -- "$tests_dir")"
config_subdir="$tests_dirname/config/clients/opc"
opc_subdir="$tests_dirname/python/clients/opc"
[ -f "$mamoma_dir/openapi.json" ]
[ -f "$mamoma_dir/$config_subdir/config.json" ]

mkdir -p -- "$mamoma_dir/$opc_subdir"
cd -- "$mamoma_dir/$opc_subdir"
rm -rf mamoma-client

"$OPC_VENV/bin/openapi-python-client" \
	generate \
	--config "$mamoma_dir/$config_subdir/config.json" \
	--path "$mamoma_dir/openapi.json" \
	--fail-on-warning
