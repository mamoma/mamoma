"""Generate the API client using Swagger."""

import os
import pathlib
import subprocess
import sys

from . import defs


def generate(
    cfg: defs.Config, spec: pathlib.Path, conffile: pathlib.Path, client: pathlib.Path
) -> pathlib.Path:
    """Generate an OpenAPI client library."""
    print(
        f"Using swagger-codegen to generate an OpenAPI client from {spec} into {client}"
    )
    subprocess.run(
        [
            "docker",
            "run",
            "--pull",
            "never",
            "--rm",
            "-it",
            f"-u{os.getuid()}:{os.getgid()}",
            "-v",
            f"{cfg.srcdir}:/opt/mamoma-source:ro",
            "-v",
            f"{client.parent}:/opt/mamoma-client",
            "--",
            "swaggerapi/swagger-codegen-cli-v3:3.0.46",
            "generate",
            "-l",
            "python",
            "-o",
            f"/opt/mamoma-client/{client.name}",
            "-i",
            f"/opt/mamoma-source/{spec.relative_to(cfg.srcdir)}",
            "-c",
            f"/opt/mamoma-source/{conffile.relative_to(cfg.srcdir)}",
        ],
        check=True,
        env=cfg.utf8_env,
    )

    if not (client / "mamoma_client/__init__.py").is_file():
        sys.exit(f"swagger-codegen did not populate {client}")

    return client
