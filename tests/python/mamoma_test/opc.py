"""Generate the API client using openapi-python-client."""

import os
import pathlib
import subprocess
import sys

from typing import Final

from . import defs


def generate(
    cfg: defs.Config, spec: pathlib.Path, conffile: pathlib.Path, client: pathlib.Path
) -> pathlib.Path:
    """Generate an OpenAPI client library."""
    venv: Final = os.environ.get("OPC_VENV")
    if venv is None:
        sys.exit(
            "No OPC_VENV at all; "
            "please point OPC_VENV to a prepared virtual environment"
        )
    bindir = pathlib.Path(venv) / "bin"
    opc_bin = bindir / "openapi-python-client"
    if not opc_bin.is_file():
        sys.exit(
            f"No {opc_bin}; please point OPC_VENV to a prepared virtual environment"
        )

    path_env = dict(cfg.utf8_env)
    path_env["PATH"] = str(bindir) + ":" + path_env["PATH"]
    print(
        f"Using openapi-python-client to generate "
        f"an OpenAPI client from {spec} into {client}"
    )
    subprocess.run(
        [
            "openapi-python-client",
            "generate",
            "--config",
            conffile,
            "--path",
            spec,
            "--fail-on-warning",
        ],
        check=True,
        cwd=client.parent,
        env=path_env,
    )

    if not (client / "mamoma_client/__init__.py").is_file():
        sys.exit(f"openapi-python-client did not populate {client}")

    return client
