"""Test a couple of API calls related to domains."""

import unittest

import mamoma_client.types as mtypes

from mamoma_client.api.default import domain_create as a_domain_create
from mamoma_client.api.default import domains_list as a_domains_list

from mamoma_client.models import domain_create
from mamoma_client.models import domain_list
from mamoma_client.models import domains_list
from mamoma_client.models import domains_list_highlight
from mamoma_client.models import error
from mamoma_client.models import req_domain_create
from mamoma_client.models import resp_domain_create_or_error
from mamoma_client.models import resp_domains_list_or_error

from .. import constants

from . import base


class TestDomains(unittest.TestCase, base.BaseTest):
    """Test some domain-related API calls."""

    def setUp(self) -> None:
        super(unittest.TestCase, self).setUp()

    def test_domains_manage(self) -> None:
        """Check whether we can list, add, and remove domains."""
        resp_list = a_domains_list.sync(client=self.api_client_auth)
        assert isinstance(resp_list, resp_domains_list_or_error.RespDomainsListOrError)
        assert resp_list.error is mtypes.UNSET
        assert isinstance(resp_list.data, domains_list.DomainsList)
        assert all(
            isinstance(item, domain_list.DomainList) for item in resp_list.data.domains
        )
        assert (
            domain_list.DomainList(
                name=constants.TEST_DOMAIN_EXISTING,
                description=constants.DESC_DOMAIN_EXISTING,
                quota_mb=2000,
                used_mb=1300,
                highlighted=False,
            )
            in resp_list.data.domains
        )
        assert all(
            item.name != constants.TEST_DOMAIN_NEW for item in resp_list.data.domains
        )

        resp_create = a_domain_create.sync(
            client=self.api_client_auth,
            form_data=req_domain_create.ReqDomainCreate(name=constants.TEST_DOMAIN_NEW),
        )
        assert isinstance(
            resp_create, resp_domain_create_or_error.RespDomainCreateOrError
        )
        assert resp_create == resp_domain_create_or_error.RespDomainCreateOrError(
            data=domain_create.DomainCreate(name=constants.TEST_DOMAIN_NEW),
            error=mtypes.UNSET,
        )

        resp_list = a_domains_list.sync(client=self.api_client_auth)
        assert isinstance(resp_list, resp_domains_list_or_error.RespDomainsListOrError)
        assert resp_list.error is mtypes.UNSET
        assert isinstance(resp_list.data, domains_list.DomainsList)
        assert all(
            isinstance(item, domain_list.DomainList) for item in resp_list.data.domains
        )
        assert (
            domain_list.DomainList(
                name=constants.TEST_DOMAIN_EXISTING,
                description=constants.DESC_DOMAIN_EXISTING,
                quota_mb=2000,
                used_mb=1300,
                highlighted=False,
            )
            in resp_list.data.domains
        )
        assert (
            domain_list.DomainList(
                name=constants.TEST_DOMAIN_NEW,
                description="",
                quota_mb=0,
                used_mb=0,
                highlighted=False,
            )
            in resp_list.data.domains
        )

    def test_domains_highlight_quota(self) -> None:
        """Check that the highlight_quota option works."""
        not_highlighted = domain_list.DomainList(
            name=constants.TEST_DOMAIN_EXISTING,
            description=constants.DESC_DOMAIN_EXISTING,
            quota_mb=2000,
            used_mb=1300,
            highlighted=False,
        )
        not_highlighted_empty = domain_list.DomainList(
            name=constants.TEST_DOMAIN_ALONLY,
            description=constants.DESC_DOMAIN_ALONLY,
            quota_mb=0,
            used_mb=0,
            highlighted=False,
        )
        highlighted = domain_list.DomainList(
            name=constants.TEST_DOMAIN_EXISTING,
            description=constants.DESC_DOMAIN_EXISTING,
            quota_mb=2000,
            used_mb=1300,
            highlighted=True,
        )
        highlighted_empty = domain_list.DomainList(
            name=constants.TEST_DOMAIN_ALONLY,
            description=constants.DESC_DOMAIN_ALONLY,
            quota_mb=0,
            used_mb=0,
            highlighted=True,
        )

        resp_list = a_domains_list.sync(client=self.api_client_auth)
        assert isinstance(resp_list, resp_domains_list_or_error.RespDomainsListOrError)
        assert resp_list.error is mtypes.UNSET
        assert isinstance(resp_list.data, domains_list.DomainsList)
        assert all(
            isinstance(item, domain_list.DomainList) for item in resp_list.data.domains
        )
        assert not_highlighted in resp_list.data.domains
        assert highlighted not in resp_list.data.domains
        assert not_highlighted_empty in resp_list.data.domains
        assert highlighted_empty not in resp_list.data.domains

        resp_list = a_domains_list.sync(
            client=self.api_client_auth,
            highlight=domains_list_highlight.DomainsListHighlight.QUOTA,
            highlight_threshold=95,
        )
        assert isinstance(resp_list, resp_domains_list_or_error.RespDomainsListOrError)
        assert resp_list.error is mtypes.UNSET
        assert isinstance(resp_list.data, domains_list.DomainsList)
        assert all(
            isinstance(item, domain_list.DomainList) for item in resp_list.data.domains
        )
        assert not_highlighted in resp_list.data.domains
        assert highlighted not in resp_list.data.domains
        assert not_highlighted_empty in resp_list.data.domains
        assert highlighted_empty not in resp_list.data.domains

        resp_list = a_domains_list.sync(
            client=self.api_client_auth,
            highlight=domains_list_highlight.DomainsListHighlight.QUOTA,
            highlight_threshold=75,
        )
        assert isinstance(resp_list, resp_domains_list_or_error.RespDomainsListOrError)
        assert resp_list.error is mtypes.UNSET
        assert isinstance(resp_list.data, domains_list.DomainsList)
        assert all(
            isinstance(item, domain_list.DomainList) for item in resp_list.data.domains
        )
        assert not_highlighted not in resp_list.data.domains
        assert highlighted in resp_list.data.domains
        assert not_highlighted_empty in resp_list.data.domains
        assert highlighted_empty not in resp_list.data.domains

    def test_domains_auth_bearer(self) -> None:
        """Check that the domain queries succeed with the bearer token."""
        resp_list = a_domains_list.sync(client=self.api_client_bearer)
        assert isinstance(resp_list, resp_domains_list_or_error.RespDomainsListOrError)
        assert resp_list.error is mtypes.UNSET
        assert not isinstance(resp_list.data, mtypes.Unset)
        assert all(
            isinstance(item, domain_list.DomainList) for item in resp_list.data.domains
        )
        assert (
            domain_list.DomainList(
                name=constants.TEST_DOMAIN_EXISTING,
                description=constants.DESC_DOMAIN_EXISTING,
                quota_mb=2000,
                used_mb=1300,
                highlighted=False,
            )
            in resp_list.data.domains
        )

    def test_domains_nonauth(self) -> None:
        """Check that the domain queries fail without authorization."""
        resp_list = a_domains_list.sync(client=self.api_client_nonauth)  # type: ignore
        assert isinstance(resp_list, resp_domains_list_or_error.RespDomainsListOrError)
        assert resp_list.data is mtypes.UNSET
        assert isinstance(resp_list.error, error.Error)
        assert resp_list.error.code == 202

        resp_create = a_domain_create.sync(
            client=self.api_client_nonauth,  # type: ignore
            form_data=req_domain_create.ReqDomainCreate(name=constants.TEST_DOMAIN_NEW),
        )
        assert isinstance(
            resp_create, resp_domain_create_or_error.RespDomainCreateOrError
        )
        assert resp_create.data is mtypes.UNSET
        assert isinstance(resp_create.error, error.Error)
        assert resp_create.error.code == 202
