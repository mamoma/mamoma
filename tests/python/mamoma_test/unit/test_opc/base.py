"""Base classes for the MaMoMa unit testing framework."""

from __future__ import annotations

import base64

import mamoma_client
import mamoma_client.types as mtypes

from mamoma_client.api.default import mmm_login
from mamoma_client.models import req_login
from mamoma_client.models import resp_login_or_error

from .. import constants


class MMMAuthClient(mamoma_client.client.AuthenticatedClient):
    """Something something something cookies."""

    def get_headers(self) -> dict[str, str]:
        """Bypass the 'Authorization' header weirdness."""
        return super(mamoma_client.client.AuthenticatedClient, self).get_headers()


# pylint: disable-next=too-few-public-methods
class BaseTest:
    """Define some common methods."""

    # pylint: disable-next=invalid-name
    def setUp(self) -> None:
        """Create the authenticated and non-authenticated API client."""

        # Create the non-authenticated API client and object.
        self.api_client_nonauth = mamoma_client.client.Client(constants.TEST_URL)

        # Prepare another API client and object for authentication.
        resp_login = mmm_login.sync(
            client=self.api_client_nonauth,
            form_data=req_login.ReqLogin(
                username=constants.MMM_USERNAME, password=constants.MMM_PASSWORD
            ),
        )
        assert isinstance(resp_login, resp_login_or_error.RespLoginOrError)
        assert not isinstance(resp_login.data, mtypes.Unset)

        self.api_client_auth = MMMAuthClient(
            constants.TEST_URL, token="", cookies={"MMM_SessionID": resp_login.data.sid}
        )

        self.api_client_bearer = mamoma_client.client.AuthenticatedClient(
            constants.TEST_URL,
            token=base64.b64encode(resp_login.data.sid.encode("UTF-8")).decode(
                "us-ascii"
            ),
        )
