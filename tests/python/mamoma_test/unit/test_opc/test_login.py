"""Make sure we can log into the MaMoMa deployment."""

import mamoma_client
import mamoma_client.types as mtypes

from mamoma_client.api.default import mmm_login
from mamoma_client.models import req_login
from mamoma_client.models import resp_login
from mamoma_client.models import resp_login_or_error

from .. import constants


def test_login() -> None:
    """Check whether we can login."""
    cli = mamoma_client.client.Client(constants.TEST_URL)
    resp = mmm_login.sync(
        client=cli,
        form_data=req_login.ReqLogin(
            username=constants.MMM_USERNAME, password=constants.MMM_PASSWORD
        ),
    )
    assert isinstance(resp, resp_login_or_error.RespLoginOrError)
    assert resp.error is mtypes.UNSET
    assert isinstance(resp.data, resp_login.RespLogin)
