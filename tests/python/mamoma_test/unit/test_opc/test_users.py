"""Test a couple of API calls related to users."""

import unittest

import mamoma_client.types as mtypes

from mamoma_client.api.default import user_create as a_user_create
from mamoma_client.api.default import users_list

from mamoma_client.models import error
from mamoma_client.models import req_user_create
from mamoma_client.models import resp_user_create_or_error
from mamoma_client.models import resp_users_list_or_error
from mamoma_client.models import user_create
from mamoma_client.models import user_list

from .. import constants

from . import base


class TestUsers(unittest.TestCase, base.BaseTest):
    """Test some user-related API calls."""

    def setUp(self) -> None:
        super(unittest.TestCase, self).setUp()

    def test_users_manage(self) -> None:
        """Check whether we can list, add, and remove users."""
        resp_list = users_list.sync(
            client=self.api_client_auth, domain=constants.TEST_DOMAIN_EXISTING
        )
        assert isinstance(resp_list, resp_users_list_or_error.RespUsersListOrError)
        assert resp_list.error is mtypes.UNSET
        assert isinstance(resp_list.data, list) and resp_list.data
        assert all(isinstance(item, user_list.UserList) for item in resp_list.data)
        uid, gid = resp_list.data[0].uid, resp_list.data[0].gid
        assert (
            user_list.UserList(
                email=constants.TEST_EMAIL_EXISTING,
                active="Y",
                quota_mb=1000,
                used_mb=400,
                uid=uid,
                gid=gid,
                home="/var/vmail/"
                + constants.TEST_DOMAIN_EXISTING
                + "/"
                + constants.TEST_ACC_EXISTING,
            )
            in resp_list.data
        )
        assert all(item.email != constants.TEST_EMAIL_NEW for item in resp_list.data)

        resp_create = a_user_create.sync(
            client=self.api_client_auth,
            form_data=req_user_create.ReqUserCreate(
                email=constants.TEST_EMAIL_NEW,
                password="another-encrypted-thing",
                active="Y",
                quota_mb=0,
            ),
        )
        assert isinstance(resp_create, resp_user_create_or_error.RespUserCreateOrError)
        assert resp_create == resp_user_create_or_error.RespUserCreateOrError(
            data=user_create.UserCreate(email=constants.TEST_EMAIL_NEW),
            error=mtypes.UNSET,
        )

        resp_list = users_list.sync(
            client=self.api_client_auth, domain=constants.TEST_DOMAIN_EXISTING
        )
        assert isinstance(resp_list, resp_users_list_or_error.RespUsersListOrError)
        assert resp_list.error is mtypes.UNSET
        assert isinstance(resp_list.data, list) and resp_list.data
        assert all(isinstance(item, user_list.UserList) for item in resp_list.data)
        uid, gid = resp_list.data[0].uid, resp_list.data[0].gid
        assert (
            user_list.UserList(
                email=constants.TEST_EMAIL_EXISTING,
                active="Y",
                quota_mb=1000,
                used_mb=400,
                uid=uid,
                gid=gid,
                home="/var/vmail/"
                + constants.TEST_DOMAIN_EXISTING
                + "/"
                + constants.TEST_ACC_EXISTING,
            )
            in resp_list.data
        )
        assert (
            user_list.UserList(
                email=constants.TEST_EMAIL_NEW,
                active="Y",
                quota_mb=0,
                used_mb=None,
                uid=uid,
                gid=gid,
                home="/var/vmail/"
                + constants.TEST_DOMAIN_EXISTING
                + "/"
                + constants.TEST_ACC_NEW,
            )
            in resp_list.data
        )

    def test_users_nonauth(self) -> None:
        """Check that the user queries fail without authorization."""
        resp_list = users_list.sync(
            client=self.api_client_nonauth,  # type: ignore
            domain=constants.TEST_DOMAIN_EXISTING,
        )
        assert isinstance(resp_list, resp_users_list_or_error.RespUsersListOrError)
        assert resp_list.data is mtypes.UNSET or resp_list.data == []
        assert isinstance(resp_list.error, error.Error)
        assert resp_list.error.code == 202
