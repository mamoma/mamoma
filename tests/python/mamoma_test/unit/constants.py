"""Common constants for the MaMoMa testing infrastructure."""

from typing import Final


TEST_HOSTNAME = "mamoma.test.ringlet.net"
TEST_URL = f"http://{TEST_HOSTNAME}/mmm.php"

MMM_USERNAME = "friend"
MMM_PASSWORD = "mellon"

TEST_DOMAIN_NEW: Final = "manage.domains.mamoma"
TEST_DOMAIN_EXISTING: Final = "users.domains.mamoma"
TEST_DOMAIN_ALONLY: Final = "forwarders.domains.mamoma"
TEST_DOMAIN_UNKNOWN: Final = "missing.domains.mamoma"

DESC_DOMAIN_EXISTING: Final = "Random user e-mail addresses"
DESC_DOMAIN_ALONLY: Final = "E-mail forwarders (aliases) only"

TEST_ACC_NEW: Final = "nobody"
TEST_ACC_EXISTING: Final = "somebody"
TEST_ACC_OVER_QUOTA: Final = "greedy"
TEST_ACC_UNKNOWN: Final = "anybody"

TEST_EMAIL_NEW: Final = TEST_ACC_NEW + "@" + TEST_DOMAIN_EXISTING
TEST_EMAIL_EXISTING: Final = TEST_ACC_EXISTING + "@" + TEST_DOMAIN_EXISTING
TEST_EMAIL_OVER_QUOTA: Final = TEST_ACC_OVER_QUOTA + "@" + TEST_DOMAIN_EXISTING
TEST_EMAIL_UNKNOWN: Final = TEST_ACC_UNKNOWN + "@" + TEST_DOMAIN_EXISTING

TEST_PASSWD_NEW: Final = "nothing"
TEST_PASSWD_EXISTING: Final = "something"
