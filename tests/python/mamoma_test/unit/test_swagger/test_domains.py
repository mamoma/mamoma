"""Test a couple of API calls related to domains."""

import unittest

import mamoma_client  # type: ignore

from .. import constants

from . import base


class TestDomains(unittest.TestCase, base.BaseTest):
    """Test some domain-related API calls."""

    def setUp(self) -> None:
        super(unittest.TestCase, self).setUp()

    def test_domains_manage(self) -> None:
        """Check whether we can list, add, and remove domains."""
        resp_list = self.api_auth.domains_list()
        assert isinstance(resp_list, mamoma_client.RespDomainsListOrError)
        assert resp_list.error is None
        assert all(
            isinstance(item, mamoma_client.DomainList)
            for item in resp_list.data.domains
        )
        assert all(
            item.name != constants.TEST_DOMAIN_NEW for item in resp_list.data.domains
        )

        resp_create = self.api_auth.domain_create(name=constants.TEST_DOMAIN_NEW)
        assert isinstance(resp_create, mamoma_client.RespDomainCreateOrError)
        assert resp_create == mamoma_client.RespDomainCreateOrError(
            data=mamoma_client.DomainCreate(name=constants.TEST_DOMAIN_NEW), error=None
        )

        resp_list = self.api_auth.domains_list()
        assert isinstance(resp_list, mamoma_client.RespDomainsListOrError)
        assert resp_list.error is None
        assert all(
            isinstance(item, mamoma_client.DomainList)
            for item in resp_list.data.domains
        )
        found = [
            item
            for item in resp_list.data.domains
            if item.name == constants.TEST_DOMAIN_NEW
        ]
        assert found == [
            mamoma_client.DomainList(
                name=constants.TEST_DOMAIN_NEW,
                description="",
                quota_mb=0,
                used_mb=0,
                highlighted=False,
            )
        ]

    def test_domains_nonauth(self) -> None:
        """Check that the domain queries fail without authorization."""
        resp_list = self.api_nonauth.domains_list()
        assert isinstance(resp_list, mamoma_client.RespDomainsListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 202

        resp_create = self.api_nonauth.domain_create(name=constants.TEST_DOMAIN_NEW)
        assert isinstance(resp_create, mamoma_client.RespDomainCreateOrError)
        assert resp_create.data is None
        assert resp_create.error.code == 202

    def test_domains_wrong_auth(self) -> None:
        """Check that the domain queries fail without authorization."""
        resp_list = self.api_wrong_auth.domains_list()
        assert isinstance(resp_list, mamoma_client.RespDomainsListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 202

        resp_create = self.api_wrong_auth.domain_create(name=constants.TEST_DOMAIN_NEW)
        assert isinstance(resp_create, mamoma_client.RespDomainCreateOrError)
        assert resp_create.data is None
        assert resp_create.error.code == 202
