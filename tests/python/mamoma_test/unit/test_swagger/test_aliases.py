"""Test a couple of API calls related to aliases."""

import unittest

import mamoma_client  # type: ignore

from .. import constants

from . import base


class TestAliases(unittest.TestCase, base.BaseTest):
    """Test some alias-related API calls."""

    def setUp(self) -> None:
        super(unittest.TestCase, self).setUp()

    def test_aliases_manage(self) -> None:
        """Check whether we can list aliases."""
        resp_list = self.api_auth.aliases_list(constants.TEST_DOMAIN_EXISTING)
        assert isinstance(resp_list, mamoma_client.RespAliasesListOrError)
        assert resp_list.error is None
        assert isinstance(resp_list.data, list)
        assert all(isinstance(item, mamoma_client.AliasList) for item in resp_list.data)
        assert all(
            item.email.endswith("@" + constants.TEST_DOMAIN_EXISTING)
            for item in resp_list.data
        )

        resp_list = self.api_auth.aliases_list(constants.TEST_DOMAIN_UNKNOWN)
        assert isinstance(resp_list, mamoma_client.RespAliasesListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 200

        resp_delete = self.api_auth.alias_delete(constants.TEST_EMAIL_UNKNOWN, 0)
        assert isinstance(resp_delete, mamoma_client.RespAliasDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 200

    def test_aliases_nonauth(self) -> None:
        """Check that the alias queries fail without authorization."""
        resp_list = self.api_nonauth.aliases_list(constants.TEST_DOMAIN_EXISTING)
        assert isinstance(resp_list, mamoma_client.RespAliasesListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 202

        resp_delete = self.api_nonauth.alias_delete(constants.TEST_EMAIL_EXISTING, 0)
        assert isinstance(resp_delete, mamoma_client.RespAliasDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202

        resp_delete = self.api_nonauth.alias_delete(constants.TEST_EMAIL_UNKNOWN, 0)
        assert isinstance(resp_delete, mamoma_client.RespAliasDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202

    def test_aliases_wrong_sid(self) -> None:
        """Check that the alias queries fail without authorization."""
        resp_list = self.api_wrong_auth.aliases_list(constants.TEST_DOMAIN_EXISTING)
        assert isinstance(resp_list, mamoma_client.RespAliasesListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 202

        resp_delete = self.api_nonauth.alias_delete(constants.TEST_EMAIL_EXISTING, 0)
        assert isinstance(resp_delete, mamoma_client.RespAliasDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202

        resp_delete = self.api_nonauth.alias_delete(constants.TEST_EMAIL_UNKNOWN, 0)
        assert isinstance(resp_delete, mamoma_client.RespAliasDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202
