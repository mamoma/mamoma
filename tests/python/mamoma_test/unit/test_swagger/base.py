"""Base classes for the MaMoMa unit testing framework."""

import mamoma_client  # type: ignore

from .. import constants


# pylint: disable-next=too-few-public-methods
class BaseTest:
    """Define some common methods."""

    # pylint: disable-next=invalid-name
    def setUp(self) -> None:
        """Create the authenticated and non-authenticated API client."""

        cfg = mamoma_client.Configuration()
        cfg.host = constants.TEST_URL
        self.api_cfg = cfg

        # Create the non-authenticated API client and object.
        self.api_client_nonauth = mamoma_client.ApiClient(configuration=cfg)
        self.api_nonauth = mamoma_client.DefaultApi(api_client=self.api_client_nonauth)

        # Prepare another API client and object for authentication.
        self.api_client_auth = mamoma_client.ApiClient(configuration=cfg)
        self.api_auth = mamoma_client.DefaultApi(api_client=self.api_client_auth)

        resp_login = self.api_auth.m_mm_login(
            username=constants.MMM_USERNAME, password=constants.MMM_PASSWORD
        )
        assert isinstance(resp_login, mamoma_client.RespLoginOrError)
        assert resp_login.data is not None
        self.api_client_auth.cookie = "MMM_SessionID=" + resp_login.data.sid

        # Prepare yet another API client and object for wrong authentication.
        self.api_client_wrong_auth = mamoma_client.ApiClient(configuration=cfg)
        self.api_wrong_auth = mamoma_client.DefaultApi(
            api_client=self.api_client_wrong_auth
        )
        self.api_client_wrong_auth.cookie = "MMM_SessionID=wrong"
