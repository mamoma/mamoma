"""Make sure we can log into the MaMoMa deployment."""

# from mamoma_client import common as mmm_common  # type: ignore
# from mamoma_client.service import mmm_login  # type: ignore

import mamoma_client  # type: ignore


def test_login() -> None:
    """Check whether we can login."""
    cfg = mamoma_client.Configuration()
    cfg.host = "http://mamoma.test.ringlet.net/mmm.php"
    api = mamoma_client.DefaultApi(mamoma_client.ApiClient(cfg))

    resp = api.m_mm_login(username="friend", password="mellony")
    assert isinstance(resp, mamoma_client.RespLoginOrError)
    assert resp.error.code == 201
    assert resp.data is None

    resp = api.m_mm_login(username="friend", password="mellon")
    assert isinstance(resp, mamoma_client.RespLoginOrError)
    assert resp.error is None
    assert resp.data is not None
