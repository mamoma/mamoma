"""Make sure we can log into the MaMoMa deployment."""

import mamoma_client  # type: ignore


def test_version() -> None:
    """Obtain the MaMoMa version string."""
    cfg = mamoma_client.Configuration()
    cfg.host = "http://mamoma.test.ringlet.net/mmm.php"
    api = mamoma_client.DefaultApi(mamoma_client.ApiClient(cfg))
    resp = api.m_mm_version()
    assert isinstance(resp, mamoma_client.RespVersionOrError)
    assert resp.error is None
    assert resp.data is not None
    assert resp.data.startswith("MaMoMa ")
