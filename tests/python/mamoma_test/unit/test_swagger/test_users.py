"""Test a couple of API calls related to users."""

import unittest

import mamoma_client  # type: ignore

from .. import constants

from . import base


class TestUsers(unittest.TestCase, base.BaseTest):
    """Test some user-related API calls."""

    def setUp(self) -> None:
        super(unittest.TestCase, self).setUp()

    def test_users_manage(self) -> None:
        """Check whether we can list users."""
        resp_list = self.api_auth.users_list(constants.TEST_DOMAIN_EXISTING)
        assert isinstance(resp_list, mamoma_client.RespUsersListOrError)
        assert resp_list.error is None
        assert isinstance(resp_list.data, list)
        assert all(isinstance(item, mamoma_client.UserList) for item in resp_list.data)
        assert all(
            item.email.endswith("@" + constants.TEST_DOMAIN_EXISTING)
            for item in resp_list.data
        )

        resp_list = self.api_auth.users_list(constants.TEST_DOMAIN_UNKNOWN)
        assert isinstance(resp_list, mamoma_client.RespUsersListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 200

        resp_delete = self.api_auth.user_delete(constants.TEST_EMAIL_UNKNOWN)
        assert isinstance(resp_delete, mamoma_client.RespUserDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 200

    def test_users_nonauth(self) -> None:
        """Check that the user queries fail without authorization."""
        resp_list = self.api_nonauth.users_list(constants.TEST_DOMAIN_EXISTING)
        assert isinstance(resp_list, mamoma_client.RespUsersListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 202

        resp_delete = self.api_nonauth.user_delete(constants.TEST_EMAIL_EXISTING)
        assert isinstance(resp_delete, mamoma_client.RespUserDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202

        resp_delete = self.api_nonauth.user_delete(constants.TEST_EMAIL_UNKNOWN)
        assert isinstance(resp_delete, mamoma_client.RespUserDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202

    def test_users_wrong_sid(self) -> None:
        """Check that the user queries fail without authorization."""
        resp_list = self.api_wrong_auth.users_list(constants.TEST_DOMAIN_EXISTING)
        assert isinstance(resp_list, mamoma_client.RespUsersListOrError)
        assert resp_list.data is None
        assert resp_list.error.code == 202

        resp_delete = self.api_nonauth.user_delete(constants.TEST_EMAIL_EXISTING)
        assert isinstance(resp_delete, mamoma_client.RespUserDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202

        resp_delete = self.api_nonauth.user_delete(constants.TEST_EMAIL_UNKNOWN)
        assert isinstance(resp_delete, mamoma_client.RespUserDeleteOrError)
        assert resp_delete.data is None
        assert resp_delete.error.code == 202
