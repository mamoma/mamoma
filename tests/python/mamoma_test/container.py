"""Handle a Docker container, start some services within it."""

from __future__ import annotations

import collections
import dataclasses
import pathlib
import subprocess
import sys
import time

from typing import DefaultDict, Final  # noqa: H301

from . import defs


@dataclasses.dataclass(frozen=True)
class Container:
    """A Docker container that was started."""

    cid: str
    utf8_env: dict[str, str]

    def get_docker_command(self, docker_command: str, command: list[str]) -> list[str]:
        """Prepend the Docker invocation to the specified command."""
        return [
            "docker",
            docker_command,
            "--",
            self.cid,
            "env",
            "LC_ALL=C.UTF-8",
            "LANGUAGE=",
        ] + command

    def run(
        self, command: list[str], *, check: bool = True
    ) -> subprocess.CompletedProcess[str]:
        """Run a command in an UTF-8 environment in the container."""
        return subprocess.run(
            self.get_docker_command("exec", command),
            encoding="UTF-8",
            check=check,
            env=self.utf8_env,
        )

    def exec_output(self, command: list[str]) -> str:
        """Execute a command in the container, return its output."""
        return subprocess.check_output(
            self.get_docker_command("exec", command),
            encoding="UTF-8",
            env=self.utf8_env,
        )


def check_proc(cont: Container, name: str, pidfile: str) -> None:
    """Check whether a process created a PID file in /run."""
    print(f"Reading a process ID from {pidfile}")
    lines = cont.exec_output(["cat", "--", pidfile]).splitlines()
    if not lines:
        sys.exit(f"Empty {pidfile} file in the {cont.cid} container")

    try:
        pid: Final = int(lines[0])
    except ValueError as err:
        sys.exit(
            f"Could not parse the {lines[0]!r} line from {pidfile} in "
            f"the {cont.cid} container: {err}"
        )

    print(f"Reading the command name for process {pid}")
    cmdfile: Final = f"/proc/{pid}/comm"
    lines = cont.exec_output(["cat", "--", cmdfile]).splitlines()
    if len(lines) != 1 or lines[0] != name:
        sys.exit(
            f"Unexpected command name for the {pid} process: "
            f"expected {name!r}, got {lines[0]!r}"
        )


@dataclasses.dataclass(frozen=True)
class ServiceInfo:
    """The commands to execute for a systemd service."""

    name: str
    pid_file: str

    exec_start_pre: list[list[str]]
    exec_start: list[list[str]]
    exec_start_post: list[list[str]]

    @classmethod
    def parse(
        cls, template: str, cont: Container, *, pid_path: str = "/run"
    ) -> ServiceInfo:
        """Parse the php*-fpm service file."""
        print(f"Looking for the {template} service file")
        lines = cont.exec_output(
            [
                "find",
                "/lib/systemd/system",
                "-mindepth",
                "1",
                "-maxdepth",
                "1",
                "-type",
                "f",
                "-name",
                f"{template}.service",
            ]
        ).splitlines()
        if len(lines) != 1:
            sys.exit(f"Unexpected `find {template}.service` output: {lines!r}")
        svcfile: Final = lines[0]
        svcname: Final = pathlib.Path(svcfile).with_suffix("").name

        print(f"Examining the contents of {svcfile}")
        lines = cont.exec_output(["cat", "--", svcfile]).splitlines()

        commands: DefaultDict[str, list[list[str]]] = collections.defaultdict(list)
        for line in lines:
            fields = line.split("=", 1)
            if len(fields) == 2:
                # Let us hope there are no funny characters in the commands
                commands[fields[0]].append(fields[1].split())

        svc = cls(
            name=svcname,
            pid_file=f"{pid_path}/{svcname}.pid",
            exec_start_pre=commands["ExecStartPre"],
            exec_start=commands["ExecStart"],
            exec_start_post=commands["ExecStartPost"],
        )

        if len(svc.exec_start) != 1:
            sys.exit(f"Expected exactly one ExecStart directive in {svcfile}")
        return svc

    def run_command(self, cont: Container, command: list[str]) -> None:
        """Run a single command, maybe do not check its return code."""
        if command[0][0] == "-":
            cont.run([command[0][1:]] + command[1:], check=False)
        else:
            cont.run(command)

    def start(
        self, cfg: defs.Config, cont: Container, *, comm: str | None = None
    ) -> None:
        """Start a service in the container."""
        pid_dir: Final = str(pathlib.Path(self.pid_file).parent)
        if pid_dir != "/run":
            print(f"Creating the {pid_dir} directory")
            cont.run(
                [
                    "install",
                    "-d",
                    "-o",
                    "www-data",
                    "-g",
                    "www-data",
                    "-m",
                    "755",
                    "--",
                    pid_dir,
                ]
            )

        print(f"Executing {len(self.exec_start_pre)} {self.name} pre-start commands")
        for cmd in self.exec_start_pre:
            print(" ".join(cmd))
            self.run_command(cont, cmd)

        print(f"Starting the {self.name} process")
        print(" ".join(self.exec_start[0]))
        proc = subprocess.Popen(  # pylint: disable=consider-using-with
            cont.get_docker_command("exec", self.exec_start[0]),
            env=cfg.utf8_env,
        )
        time.sleep(1)

        print(f"Executing {len(self.exec_start_post)} {self.name} post-start commands")
        for cmd in self.exec_start_post:
            print(" ".join(cmd))
            self.run_command(cont, cmd)

        print("Waiting for a while")
        time.sleep(1)
        if proc.poll() is not None:
            sys.exit(f"The {self.name} process exited with code {proc.wait()}")
        check_proc(cont, comm if comm is not None else self.name, self.pid_file)
