"""Common definitions for the MaMoMa test suite."""

from __future__ import annotations

import dataclasses
import pathlib


@dataclasses.dataclass(frozen=True)
class Config:
    """Common configuration for the different implementations."""

    srcdir: pathlib.Path
    utf8_env: dict[str, str]


VERSION = "0.1.0"
