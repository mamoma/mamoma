"""Configure and start some services within the container."""

from __future__ import annotations

import json
import pathlib
import re
import subprocess
import sys

from typing import Final, Sequence  # noqa: H301

from .unit import constants

from . import container
from . import defs


_RE_LOCALHOST_IP_ADDRESS: Final = re.compile(r" ^ [0127.:]+ $", re.X)
_RE_FPM_NAME: Final = re.compile(r" ^ php (?P<version> [0-9.]+ ) -fpm $ ", re.X)

_DB_NAME: Final = "mamoma"
_DB_USERNAME: Final = "mamoma"
_DB_PASSWORD: Final = "nothing"


def fixup_hosts_file(cont: container.Container) -> None:
    """Add our test hostname to the localhost line."""
    print("Looking for localhost lines in the container's hosts file")
    lines = cont.exec_output(
        ["grep", "-w", "-e", "localhost", "--", "/etc/hosts"]
    ).splitlines()
    if not lines:
        sys.exit("Could not find any localhost lines in the container")

    found = False
    for line in lines:
        fields = line.split()
        if "localhost" not in fields:
            continue
        address = fields[0]
        if not _RE_LOCALHOST_IP_ADDRESS.match(address):
            print(f"Skipping {address!r}")
            continue

        print(f"Adding {address!r} to the hosts file")
        cont.run(
            [
                "sh",
                "-c",
                f"echo '{address} {constants.TEST_HOSTNAME}' >> /etc/hosts",
            ],
            check=True,
        )
        found = True

    if not found:
        sys.exit(f"No valid IP addresses for localhost found among {lines!r}")

    cont.run(["cat", "/etc/hosts"], check=True)


def generate_mamoma_conf(
    mamoma_conf: pathlib.Path, cont: container.Container
) -> tuple[int, int]:
    """Generate the MaMoMa config file for this environment."""
    print("Obtaining the user and group ID of the www-data account")
    lines: Final[Sequence[str]] = cont.exec_output(
        ["getent", "passwd", "www-data"]
    ).splitlines()
    if len(lines) != 1:
        sys.exit(f"Unexpected number of lines: {lines!r}")
    line: Final = lines[0]
    fields: Final[Sequence[str]] = line.split(":")
    if len(fields) < 4:
        sys.exit(f"Unexpected number of fields: {line!r}")
    try:
        uid: Final = int(fields[2])
        gid: Final = int(fields[3])
    except ValueError:
        sys.exit(f"Invalid user or group ID: {line!r}")
    print(f"Got user ID {uid}, group ID {gid}")

    config_text: Final = f"""
[db]
db          = 'mysql:host=localhost;dbname={_DB_NAME}'
username    = '{_DB_USERNAME}'
password    = '{_DB_PASSWORD}'

[fs]
basedir     = /var/vmail
uid         = {uid}
gid         = {gid}
"""
    mamoma_conf.write_text(config_text, encoding="UTF-8")
    print("Checking whether the container sees the MaMoMa config file")
    contents: Final = cont.exec_output(["cat", "--", "/opt/mamoma/conf/mmm.conf"])
    if contents != config_text:
        sys.exit(f"Expected {config_text!r}, got {contents!r}")
    print(contents)

    return uid, gid


def prepare_container(
    cfg: defs.Config,
    tempd: pathlib.Path,
    mamoma_conf: pathlib.Path,
    cont: container.Container,
) -> tuple[int, int]:
    """Install some packages into the container."""
    fixup_hosts_file(cont)

    uid, gid = generate_mamoma_conf(mamoma_conf, cont)

    print("Preparing the lighttpd config file")
    tempf_name: Final = "98-mamoma-vhost.conf"
    tempf: Final = tempd / tempf_name
    tempf.write_text(
        f"""
$HTTP["host"] =~ "{constants.TEST_HOSTNAME}" {{
    server.document-root = "/opt/mamoma/static"
}}
""",
        encoding="UTF-8",
    )

    print("Copying it over to the container")
    subprocess.run(
        [
            "docker",
            "cp",
            "--",
            tempf,
            f"{cont.cid}:/etc/lighttpd/conf-enabled/{tempf_name}",
        ],
        check=True,
        env=cfg.utf8_env,
    )

    return uid, gid


def start_db(cfg: defs.Config, cont: container.Container, uid: int, gid: int) -> None:
    """Start the database server, prepare the MaMoMa config file."""
    # This one requires way too many environment variables and fixups
    print("Starting the MariaDB server")
    cont.run(["/etc/init.d/mariadb", "start"])

    print("Making sure the MariaDB server works")
    lines = cont.exec_output(["mariadb", "-e", "\\s"]).splitlines()
    found = [
        line
        for line in lines
        if line.startswith("Current user:") and "root@localhost" in line
    ]
    if not found:
        sys.exit(f"Could not find a current user root line in {lines!r}")

    print("Creating the MaMoMa database")
    cont.run(["mariadb", "-e", f'CREATE DATABASE {_DB_NAME} CHARSET "utf8"'])
    cont.run(
        [
            "mariadb",
            "-e",
            f"GRANT ALL ON {_DB_NAME}.* "
            f'TO "{_DB_USERNAME}"@"localhost" '
            f'IDENTIFIED BY "{_DB_PASSWORD}"',
        ]
    )

    print("Checking whether the database authentication works")
    lines = cont.exec_output(
        [
            "mariadb",
            f"-u{_DB_USERNAME}",
            f"-p{_DB_PASSWORD}",
            "-B",
            "-N",
            "-e",
            "SHOW TABLES",
            "--",
            _DB_NAME,
        ]
    ).splitlines()
    if lines:
        sys.exit(f"Did not expect any tables in the mamoma database: {lines!r}")
    lines = cont.exec_output(
        [
            "mariadb",
            f"-u{_DB_USERNAME}",
            f"-p{_DB_PASSWORD}",
            "-B",
            "-N",
            "-e",
            "SHOW DATABASES",
            "--",
            _DB_NAME,
        ]
    ).splitlines()
    if set(lines) != set(("information_schema", "mamoma")):
        sys.exit(f"Unexpected show databases output: {lines!r}")

    print("Executing the MaMoMa database init scripts")
    for name in sorted(
        path.name
        for path in (cfg.srcdir / "db").iterdir()
        if path.is_file() and path.name.endswith(".sql")
    ):
        print(f"- {name}")
        cont.run(
            [
                "mariadb",
                f"-u{_DB_USERNAME}",
                f"-p{_DB_PASSWORD}",
                "-B",
                "--abort-source-on-error",
                "-e",
                f"BEGIN;\n\\. /opt/mamoma/db/{name}\nCOMMIT;",
                "--",
                _DB_NAME,
            ]
        )

    print("Creating a MaMoMa test user account")
    cont.run(
        [
            "mariadb",
            f"-u{_DB_USERNAME}",
            f"-p{_DB_PASSWORD}",
            "-B",
            "-e",
            "INSERT INTO mmm_users(name, username, password, admin) VALUES("
            f'"Test user account", "{constants.MMM_USERNAME}", '
            '"$6$O7RarID42D.tui8v$XWPBIMBJ.vUjR185pNX.rQhkTYQePao9sxdV5hHZ4xqjuggwvg9'
            'JnrJPEZoIUd6CsAAsDl/cigj36B4qEW1AG1", "Y")',
            "--",
            _DB_NAME,
        ]
    )

    print(f"Creating the {constants.TEST_DOMAIN_EXISTING} e-mail domain")
    cont.run(
        [
            "mariadb",
            f"-u{_DB_USERNAME}",
            f"-p{_DB_PASSWORD}",
            "-B",
            "-e",
            "INSERT INTO virtual_domains(name, description) VALUES "
            f'("{constants.TEST_DOMAIN_EXISTING}", '
            f'"{constants.DESC_DOMAIN_EXISTING}"), '
            f'("{constants.TEST_DOMAIN_ALONLY}", '
            f'"{constants.DESC_DOMAIN_ALONLY}")'
            "--",
            _DB_NAME,
        ]
    )

    print("Obtaining the database ID of the just-created e-mail domain")
    lines = cont.exec_output(
        [
            "mariadb",
            f"-u{_DB_USERNAME}",
            f"-p{_DB_PASSWORD}",
            "-B",
            "-N",
            "-e",
            f"SELECT id FROM virtual_domains "
            f'WHERE name = "{constants.TEST_DOMAIN_EXISTING}"',
            "--",
            _DB_NAME,
        ]
    ).splitlines()
    if len(lines) != 1:
        sys.exit(f"Unexpected 'select id from virtual_domains' output: {lines!r}")
    try:
        domain_id = int(lines[0])
    except ValueError as err:
        sys.exit(f"Unexpected domain ID {lines[0]!r}: {err}")
    if lines[0] != str(domain_id):
        sys.exit(f"Unexpected domain ID {lines[0]!r}")

    print(f"Creating the {constants.TEST_EMAIL_EXISTING} e-mail account")
    cont.run(
        [
            "mariadb",
            f"-u{_DB_USERNAME}",
            f"-p{_DB_PASSWORD}",
            "-B",
            "-e",
            "INSERT INTO virtual_users(domain_id, active, email, password, quota_kb, "
            "used_kb, home, uid, gid) "
            f'VALUES({domain_id}, "Y", "{constants.TEST_EMAIL_EXISTING}", '
            f'"encrypted-stuff", 1000 * 1024, 400 * 1024, '
            f'"/var/vmail/{constants.TEST_DOMAIN_EXISTING}/'
            f'{constants.TEST_ACC_EXISTING}", {uid}, {gid}), '
            f'({domain_id}, "Y", "{constants.TEST_EMAIL_OVER_QUOTA}", '
            f'"encrypted-stuff", 1000 * 1024, 900 * 1024, '
            f'"/var/vmail/{constants.TEST_DOMAIN_EXISTING}/'
            f'{constants.TEST_ACC_OVER_QUOTA}", {uid}, {gid})'
            "--",
            _DB_NAME,
        ]
    )


def start_lighttpd(
    cfg: defs.Config, cont: container.Container, svc: container.ServiceInfo
) -> None:
    """Start the lighttpd webserver and make sure it works."""
    svc.start(cfg, cont)

    print("Checking whether lighttpd accepts requests")
    cont.run(
        [
            "curl",
            "-s",
            "-I",
            "http://127.0.0.1/",
        ]
    )

    print("Checking whether lighttpd accepts requests at our vhost")
    contents: Final = cont.exec_output(
        [
            "curl",
            "-s",
            f"http://{constants.TEST_HOSTNAME}/css/mmm.css",
        ]
    )
    if contents != (cfg.srcdir / "static/css/mmm.css").read_text(encoding="UTF-8"):
        sys.exit(f"Wrong contents for mmm.css: {contents!r}")


def start_php_fpm(
    cfg: defs.Config, cont: container.Container, svc: container.ServiceInfo
) -> None:
    """Start the php-fpm manager and make sure it works."""
    fpm_data: Final = _RE_FPM_NAME.match(svc.name)
    if not fpm_data:
        sys.exit(f"Unexpected php-fpm service name {svc.name!r}")
    fpm_ver: Final = fpm_data.group("version")
    svc.start(cfg, cont, comm=f"php-fpm{fpm_ver}")

    print("Checking whether lighttpd executes PHP files")
    contents = cont.exec_output(
        [
            "curl",
            "-s",
            "-X",
            "POST",
            "-d",
            "",
            f"{constants.TEST_URL}/MMMLogin",
        ]
    )
    try:
        data = json.loads(contents)
    except ValueError:
        sys.exit(f"Could not parse a valid JSON response out of {contents!r}")
    if not isinstance(data, dict) or "error" not in data:
        sys.exit(f"Expected a MaMoMa error response: {contents!r}")

    print("Checking whether MaMoMa can process a valid login request")
    contents = cont.exec_output(
        [
            "curl",
            "-s",
            "-X",
            "POST",
            "-d",
            f"username={constants.MMM_USERNAME}&password={constants.MMM_PASSWORD}",
            f"{constants.TEST_URL}/MMMLogin",
        ]
    )
    try:
        data = json.loads(contents)
    except ValueError:
        sys.exit(f"Could not parse a valid JSON response out of {contents!r}")
    if not isinstance(data, dict) or "data" not in data:
        sys.exit(f"Expected a correct MaMoMa login response: {contents!r}")
