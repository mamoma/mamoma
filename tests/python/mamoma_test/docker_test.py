#!/usr/bin/python3
"""Run MaMoMa tests in a Docker environment."""

from __future__ import annotations

import argparse
import dataclasses
import pathlib
import re
import subprocess
import sys
import tempfile

from typing import Callable, Final, Sequence  # noqa: H301

import utf8_locale

from . import container
from . import defs
from . import opc
from . import services
from . import swagger


_DEF_IMAGE: Final = "mamoma:bullseye"

_RE_CID: Final = re.compile(r" ^ [0-9a-fA-F]+ $ ", re.X)


@dataclasses.dataclass(frozen=True)
class Impl:
    """The properties of the OpenAPI implementation to use."""

    generate: Callable[
        [defs.Config, pathlib.Path, pathlib.Path, pathlib.Path], pathlib.Path
    ]
    slug: str
    test_skip: str


_IMPL = {
    "opc": Impl(generate=opc.generate, slug="opc", test_skip="test_swagger"),
    "swagger": Impl(generate=swagger.generate, slug="swagger", test_skip="test_opc"),
}


@dataclasses.dataclass(frozen=True)
class Config(defs.Config):
    """Runtime configuration for the MaMoMa Docker tests."""

    image: str
    impl: Impl
    skip_generate: bool


def parse_args() -> Config:
    """Parse the command-line arguments."""
    parser: Final = argparse.ArgumentParser(prog="mamoma_docker")
    parser.add_argument(
        "-G",
        "--skip-generate",
        action="store_true",
        help="use the pregenerated clients in the source tree",
    )
    parser.add_argument(
        "-i", "--image", type=str, default=_DEF_IMAGE, help="the Docker image to use"
    )
    parser.add_argument(
        "-I",
        "--impl",
        type=str,
        choices=sorted(_IMPL.keys()),
        required=True,
        help="the OpenAPI implementation to test",
    )

    args: Final = parser.parse_args()

    srcdir: Final = pathlib.Path(".").absolute().parent
    if (
        not (srcdir / "openapi.json").is_file()
        or not (srcdir / "static/mmm.php").is_file()
    ):
        sys.exit(f"No MaMoMa source at {srcdir}")

    return Config(
        image=args.image,
        impl=_IMPL[args.impl],
        skip_generate=args.skip_generate,
        srcdir=srcdir,
        utf8_env=utf8_locale.get_utf8_env(),
    )


def start_container(
    cfg: Config, mamoma_dir: pathlib.Path, client: pathlib.Path
) -> tuple[container.Container, pathlib.Path]:
    """Start a Docker container."""
    mamoma_conf: Final = mamoma_dir / "conf/mmm.conf"
    mamoma_conf.write_text("", encoding="UTF-8")

    print(f"Starting a Docker container, mounting {mamoma_dir!r} and {client!r}")
    lines: Final[Sequence[str]] = subprocess.check_output(
        [
            "docker",
            "run",
            "--pull",
            "never",
            "--detach",
            "--init",
            "--rm",
            "-v",
            f"{mamoma_dir}:/opt/mamoma:ro",
            "-v",
            f"{client}:/opt/mamoma_client:ro",
            "--",
            cfg.image,
            "sleep",
            "3600",
        ],
        encoding="UTF-8",
        env=cfg.utf8_env,
    ).splitlines()
    if len(lines) != 1 or not _RE_CID.match(lines[0]):
        sys.exit(f"Unexpected `docker run` output: {lines!r}")
    return container.Container(cid=lines[0], utf8_env=cfg.utf8_env), mamoma_conf


def prepare_tests_venv(cont: container.Container) -> str:
    """Prepare the virtual environment for the tests, return PYTHONPATH."""
    print("Copying the mamoma-client source to a temporary directory")
    cont.run(["cp", "-R", "--", "/opt/mamoma_client", "/tmp/mamoma-client-build"])

    print("Preparing the virtual environment for the tests")
    vpath = "/opt/mamoma_test_venv"
    cont.run(["python3", "-m", "venv", vpath])
    cont.run(
        [
            f"{vpath}/bin/python3",
            "-m",
            "pip",
            "install",
            "/tmp/mamoma-client-build/",
            "pytest",
        ]
    )
    return vpath


def generate(cfg: Config, tempd: pathlib.Path) -> pathlib.Path:
    """Generate the OpenAPI Python client using the selected method."""
    client: Final = tempd / "cli/mamoma-client"
    client.parent.mkdir(mode=0o755)

    if cfg.skip_generate:
        cbase = cfg.srcdir / "tests/python/clients" / cfg.impl.slug
        for rel in ("mamoma-client", "mamoma_client"):
            source = cbase / rel
            if source.is_dir():
                break
        else:
            sys.exit(f"Could not find mamoma-client or mamoma_client in {cbase}")

        client.mkdir(mode=0o755)
        target = client / source.name
        print(f"Copying {source} to {target}")
        subprocess.check_call(
            ["rsync", "-av", "--", f"{source}/", f"{target}/"], env=cfg.utf8_env
        )
        return target

    spec: Final = cfg.srcdir / "openapi.json"
    conffile: Final = (
        cfg.srcdir / "tests/config/clients" / cfg.impl.slug / "config.json"
    )
    if not spec.is_file() or not conffile.is_file():
        sys.exit(f"Need both {spec} and {conffile} to be regular files")

    if client.exists() or client.is_symlink():
        sys.exit(f"Did not expect {client} to exist")

    return cfg.impl.generate(cfg, spec, conffile, client)


def main() -> None:
    """Parse command-line parameters, run tests."""
    cfg: Final = parse_args()
    with tempfile.TemporaryDirectory() as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)
        print(f"Using {tempd} as a temporary directory")
        mamoma_dir: Final = tempd / "mamoma"
        print(f"Copying {cfg.srcdir} into {mamoma_dir}")
        subprocess.run(
            [
                "rsync",
                "-a",
                "--exclude",
                ".git",
                "--exclude",
                ".tox",
                "--",
                f"{cfg.srcdir}/",
                f"{mamoma_dir}/",
            ],
            check=True,
        )

        client: Final = generate(cfg, tempd)
        cont = None
        try:
            cont, mamoma_conf = start_container(cfg, mamoma_dir, client)
            print(f"Started a Docker container: {cont.cid}")
            uid, gid = services.prepare_container(cfg, tempd, mamoma_conf, cont)
            lighttpd_svc: Final = container.ServiceInfo.parse("lighttpd", cont)
            print(repr(lighttpd_svc))
            fpm_svc: Final = container.ServiceInfo.parse(
                "php*-fpm", cont, pid_path="/run/php"
            )
            print(repr(fpm_svc))

            services.start_db(cfg, cont, uid, gid)
            services.start_lighttpd(cfg, cont, lighttpd_svc)
            services.start_php_fpm(cfg, cont, fpm_svc)

            venv_path: Final = prepare_tests_venv(cont)

            print("Running some tests now")
            try:
                cont.run(
                    [
                        f"{venv_path}/bin/pytest",
                        "--ignore",
                        (
                            f"/opt/mamoma/tests/python/mamoma_test/"
                            f"unit/{cfg.impl.test_skip}"
                        ),
                        "-k",
                        f"test_{cfg.impl.slug}",
                        "--",
                        "/opt/mamoma/tests/python/mamoma_test/unit",
                    ]
                )
            except subprocess.CalledProcessError:
                print("The pytest invocation failed, dumping the lighttpd error log")
                cont.run(["cat", "/var/log/lighttpd/error.log"])
                raise
        finally:
            if cont is not None:
                print(f"Stopping the {cont.cid} container")
                try:
                    subprocess.run(
                        ["docker", "stop", "--", cont.cid],
                        check=True,
                        env=cfg.utf8_env,
                    )
                except subprocess.CalledProcessError as err:
                    print(
                        f"Could not stop the {cont.cid!r} Docker container: {err}",
                        file=sys.stderr,
                    )


if __name__ == "__main__":
    main()
