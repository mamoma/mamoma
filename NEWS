Change log for Manage Mo' Mail, the e-mail domain control panel.

2.0.0.b6	not yet
	- send an application/json HTTP response
	- add an EditorConfig definitions file
	- add the virtual_domains.description column to the sample
	  database creation SQL scripts
	- add an incomplete OpenAPI schema and a Docker test toolset
	  (this needs a lot more documentation and also completeness)
	- implement RFC6750 Bearer tokens as an auth method
	- add the "highlight" and "highlight_threshold" optional
	  parameters to the DomainsList query and return highlight: true
	  for domains that have at least one user account exceeding
	  the specified quota threshold percentage
	- highlight such domains in the web UI list
	- add the MMMFeatures query for supported features
	- add type declarations to the PHP source
	- reformat the SQL queries to make them more readable
	- do not attempt to use persistent database connections by default;
	  the new "persistent" boolean parameter in the "db" section of
	  the configuration file may be used to restore the old behavior

2.0.0.b5	2018-01-03
	- properly delete accounts with usernames containing dots

2.0.0.b4	2017-08-10
	INCOMPATIBLE CHANGES:
	- make DomainsList return a data structure with the value of
	  the new configuration field, fs.available_mb, and a list of
	  objects with more data
	- add a new configuration field, fs.quota_kb, for a default quota;
	  if it is not specified, an empty quota field will be treated as
	  an error
	- add a new virtual_domains.description varchar(20) database column
	  and display it along with the domain name in the domains list

	Other additions and bugfixes:
	- display the list of domains in two columns
	- make a lot of variables local
	- fetch PDO data as associative arrays by default
	- list domains' total quota and used space
	- make the user ID, group ID, and home directory fields read-only
	- remove some success notification dialogs
	- add a @domain hint when creating a user account or an alias
	- shorten all the text entry fields by half
	- report the percentage of the quota assigned and the space used in
	  the list of domains and the list of users for a domain
	- escape the HTML entities in the domain name and description in
	  the JSON API's DomainsList query
	- do not log a warning on RESTful invocations with the action name
	  passed in the path and not as a query parameter
	- do not show the "Used" field at all when creating a new account
	- align numeric columns to the right
	- properly handle usernames containing dots
	- support the new doveadm quota output format
	- correct the order of arguments in the doveadm invocation

1.0.0	2016-03-28
	INCOMPATIBLE CHANGES:
	- expose the user quota in megabytes, not kilobytes
	- support multiple username@domain aliases by passing the alias's
	  internal ID along with the source address in pretty much all
	  the alias actions

	Other additions and bugfixes:
	- add the MMM_DomainCreate() function
	- add the DomainCreate query to create a domain by name
	- add the MMM_ERR_NOT_ADMIN error code
	- add the db/01-internal.sql initial database schema
	- add the db/02-quota.sql schema for the "quota" table and
	  display the actual amount of disk space used by each account
	- add the db/03-quota-in-vusers.sql schema update to move
	  the "used_kb" field to the virtual_users table
	- remove some diagnostic log statements to the JavaScript console
	- do not animate the alert and notification dialogs
	- add the mamoma-dovecot-used-quota tool (implemented in both
	  Perl 5 and Perl 6) to fill in the virtual_users.used_db field

0.2.0	2014-04-02
	- add a Makefile with a 'release' target
	- fix a first-use bug: return an 'invalid session ID' error code
	  when no session ID has been sent at all so that no error dialog
	  is shown in the web interface
	- implement array_column() if it does not exist (e.g. PHP < 5.5.0)
	- fix a UI problem that manifested as a failure to log in:
	  mmm_wrap_err() and mmm_wrap_ui() return false now if the callback
	  forgets to actually return a value
	- document some of the mmm.js functions
	- really hide the UI user pages when "back to..." is clicked
	- in the UI, 'return false' in all onclick events
	- make the UI glyphs and text spans look like clickable links
	- implement creating, updating and removing aliases

0.01	2014-02-23
	- first semi-public release :)

Contact: Peter Pentchev <roam@ringlet.net>
