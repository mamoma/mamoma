#!/usr/bin/make -f
#
# Copyright (c) 2014  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# This Makefile currently only provides the 'release' target to create
# a release distribution of MaMoMa.
#
# At some point in the future there might be an 'install' target, too,
# to be used from the release distribution directory, but first I'll have
# to figure out exactly what it is supposed to do :)

all:
	@echo "No build necessary for MaMoMa at this point."

release:
	ver=`sh tools/mamoma-version.sh` && [ -n "$$ver" ] && ${MAKE} do-release MAMOMA_VERSION="$$ver"

release_parent?=	..
release_dir?=		${release_parent}/${MAMOMA_VERSION}

do-release:
	if [ -z "${MAMOMA_VERSION}" ]; then \
		echo 'No MAMOMA_VERSION in the do-release target; build toolchain problem?' 1>&2; \
		false; \
	fi
	if [ -e "${release_dir}" ]; then \
		echo "The MaMoMa release directory ${release_dir} already exists!" 1>&2; \
		false; \
	fi
	mkdir "${release_dir}"
	git ls-files | egrep -ve '^contrib/' | while read f; do \
		if [ ! -f "$$f" ]; then \
			continue; \
		fi; \
		d=`dirname "$$f"`; \
		mkdir -p "${release_dir}/$$d"; \
		install -c -m 644 "$$f" "${release_dir}/$$f"; \
	done
	find "${release_dir}"/ -type d -print0 | xargs -0 chmod 755

.PHONY:	all release do-release
