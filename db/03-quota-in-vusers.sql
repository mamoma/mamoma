ALTER TABLE virtual_users ADD COLUMN used_kb INTEGER;

UPDATE virtual_users
       INNER JOIN quota ON quota.email = virtual_users.email
  SET virtual_users.used_kb = quota.used_kb;

RENAME TABLE quota TO quota_xxx;
