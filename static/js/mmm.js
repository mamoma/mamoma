/*-
 * Copyright (c) 2014, 2016 - 2018, 2022  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

var mmm_domain = "(none)";
var mmm_users = {};
var mmm_user = "(none)";
var mmm_aliases = {};
var mmm_alias_id = -1;
var mmm_alias = "(none)";
var mmm_inDialog = false;
var mmm_loggedIn = false;

var highlight_threshold = 85;

function mmm_top_only(id) {
	$('div.mmm-top').hide();
	if (id != null)
		$('#mmm-top-' + id).show();
	if (mmm_loggedIn) {
		$('.mmm-logged-in').show();
		$('.mmm-not-logged-in').hide();
	} else {
		$('.mmm-logged-in').hide();
		$('.mmm-not-logged-in').show();
	}
}

/**
 * Function:
 *	mmm_wrap_err()		- catch and log/display errors
 * Inputs:
 *	funcname		- the "caller ID" to be used in messages
 *	callback		- the code to invoke and catch errors from
 * Returns:
 *	The return value of the callback if any, false otherwise.
 * Modifies:
 *	Nothing by itself; invokes the callback, possibly logs error messages
 *	and stack traces to the console and displays an error message alert.
 */
function mmm_wrap_err(funcname, callback) {
	try {
		var val = callback();
		return val != null? val: false;
	} catch (e) {
		console.log(e);
		console.log(e.stack); // RDBG
		alert('RDBG ' + funcname + '  error: ' + e);
		return false;
	}
}

/**
 * Function:
 *	mmm_wrap_ui()		- only invoke MaMoMa UI routines when safe
 * Inputs:
 *	funcname		- the "caller ID" to be used in messages
 *	callback		- the code to invoke and catch errors from
 * Returns:
 *	The return value of the callback if any, false otherwise.
 * Modifies:
 *	Nothing by itself.
 *	Does nothing if a MaMoMa dialog is currently being shown.
 *	Invokes mmm_show_login() instead of the callback if the user has not
 *	logged in yet.
 *	Invokes the callback via mmm_wrap_err() otherwise.
 */
function mmm_wrap_ui(funcname, callback) {
	return mmm_wrap_err(funcname, function() {
		if (mmm_inDialog)
			return false;
		if (!mmm_loggedIn)
			return mmm_show_login();

		var val = callback();
		return val != null? val: false;
	});
}

/**
 * Function:
 *	mmm_request		- send a GET or POST request to the MaMoMa API
 * Inputs:
 *	type			- "GET" or "POST"
 *	query			- the MaMoMa API query name, e.g. "DomainsList"
 *	params			- a var: value array of API query parameters
 *	callback		- the function to call on request completion
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself, invokes $.get() or $.post().
 */
function mmm_request(type, query, params, callback) {
	var url = 'mmm.php?mmm=' + encodeURIComponent(query);
	if (type == 'GET')
		$.get(url, params).always(
		    function(r, s, x) {
			cb_mmm_generic(r, s, x, callback)
		});
	else if (type == 'POST')
		$.post(url, params).always(
		    function(r, s, x) {
			cb_mmm_generic(r, s, x, callback)
		});
	else
		throw 'MaMoMa internal error: mmm_request() invoked with '
		    + 'invalid type "' + type + '"';
}

/**
 * Function:
 *	mmm_get			- send a GET request to the MaMoMa API
 * Inputs:
 *	query			- the MaMoMa API query name, e.g. "DomainsList"
 *	params			- a var: value array of API query parameters
 *	callback		- the function to call on request completion
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself, invokes mmm_request('GET', ...).
 */
function mmm_get(query, params, callback) {
	mmm_request('GET', query, params, callback);
}

/**
 * Function:
 *	mmm_post		- send a POST request to the MaMoMa API
 * Inputs:
 *	query			- the MaMoMa API query name, e.g. "DomainsList"
 *	params			- a var: value array of API query parameters
 *	callback		- the function to call on request completion
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself, invokes mmm_request('POST', ...).
 */
function mmm_post(query, params, callback) {
	mmm_request('POST', query, params, callback);
}

/**
 * Function:
 *	mmm_login		- send a MaMoMa API login request
 * Inputs:
 *	None.
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a MMMLogin request using the username and
 *	password entered on the mmm-top-login form.
 */
function mmm_login() {
	return mmm_wrap_err('mmm_login', function() {
		if (mmm_inDialog)
			return false;
		$('#mmmFormLoginErrMsg').hide();
		var username = $('#mmmFormLoginUsername').val();
		var password = $('#mmmFormLoginPassword').val();
		mmm_post('MMMLogin',
		    { username: username, password: password },
		    cb_mmm_login);
	});
}

function mmm_logged_out() {
	mmm_loggedIn = false;
	$('#mmmGreeting').html("We are the Bor&#8230;, err, I mean, the Manage Mo' Mail system.  Who are you?");
	mmm_show_login();
}

function cb_mmm_logout(data, err) {
	return mmm_wrap_err('cb_mmm_logout', function() {
		if (err != null)
			return mmm_notice('Logging out failed: ' + err.message);

		mmm_logged_out();
	});
}

/**
 * Function:
 *	mmm_logout		- send a MaMoMa API logout request
 * Inputs:
 *	None.
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a MMMLogout request.
 */
function mmm_logout() {
	return mmm_wrap_ui('mmm_logout', function() {
		mmm_post('MMMLogout', {}, cb_mmm_logout);
	});
}

/**
 * Function:
 *	mmm_list_domains	- send a MaMoMa API "list domains" request
 * Inputs:
 *	None.
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a DomainsList request and displays
 *	the domains list section of the UI.
 */
function mmm_list_domains() {
	return mmm_wrap_ui('mmm_list_domains', function() {
		$('#mmm-domains-table').children('[id!="mmm-domains--template"]').remove();
		$('#mmm-domains-label').text('Fetching the list of domains...');
		$('#mmm-domains-err-msg').hide();
		mmm_top_only('domains');

		mmm_get('DomainsList',
			{'highlight': 'quota', 'highlight_threshold': highlight_threshold},
			cb_mmm_list_domains);
	});
}

/**
 * Function:
 *	mmm_list_users		- send a MaMoMa API "list users" request
 * Inputs:
 *	domain			- the name of the domain selected by the user
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a UsersList request for the domain selected by
 *	the user and displays the domains list section of the UI.
 */
function mmm_list_users(domain) {
	return mmm_wrap_ui('mmm_list_users', function() {
		$('#mmm-users-table').children('[id!="mmm-users--template"]').remove();
		$('#mmm-users-err-msg').hide();
		$('#mmm-users-label').text('Fetching the users for domain ' + mmm_domain + '...');
		mmm_top_only('users');

		mmm_get('UsersList', { domain: domain }, cb_mmm_list_users);
	});
}

/**
 * Function:
 *	mmm_list_aliases	- send a MaMoMa API "list aliases" request
 * Inputs:
 *	domain			- the name of the domain selected by the user
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a AliasesList request for the domain selected by
 *	the user and displays the domains list section of the UI.
 */
function mmm_list_aliases(domain) {
	return mmm_wrap_ui('mmm_list_aliases', function() {
		$('#mmm-aliases-table').children('[id!="mmm-aliases--template"]').remove();
		$('#mmm-aliases-err-msg').hide();
		$('#mmm-aliases-label').text('Fetching the aliases for domain ' + mmm_domain + '...');
		mmm_top_only('aliases');

		mmm_get('AliasesList', { domain: domain }, cb_mmm_list_aliases);
	});
}

function mmm_logged_in(data) {
	return mmm_wrap_err('mmm_logged_in', function() {
		mmm_loggedIn = true;
		$('#mmmGreeting').text('Welcome, ' + data.name + '!');
		$('#mmm-logout-username').text(data.username);
		mmm_list_domains();
	});
}

function cb_mmm_dialog_generic(func) {
	if (func != null)
		return function(arg) { mmm_inDialog = false; func(); }
	else
		return function(arg) { mmm_inDialog = false; }
}

function mmm_error(id, err) {
	$('#' + id).text(err);
	$('#' + id).show();
	mmm_inDialog = true;
	bootbox.alert({
	    message: err,
	    callback: cb_mmm_dialog_generic(null),
	    animate: false
	});
}

function mmm_notice(msg, func) {
	mmm_inDialog = true;
	bootbox.alert({
	    message: msg,
	    callback: cb_mmm_dialog_generic(func),
	    animate: false
	});
}

function cb_mmm_login(data, err) {
	if (err !== null) {
		if (err.code == 201)
			err = 'MaMoMa login error: ' + err.message;
		else
			err = 'The MaMoMa server could not complete the login request';
		mmm_error('mmmFormLoginErrMsg', err);
		return;
	}

	mmm_logged_in(data);
}

function me(code, message) {
	return { code: code, message: message };
}

function me1(message) {
	return me(1, message);
}

function cb_mmm_generic(resp, textStatus, jqXHR, callback) {
	function process(data, err) {
		if (data === null) {
			if (err === null)
				err = me1('The MaMoMa server returned an empty JSON response');
		} else if (data.hasOwnProperty('data')) {
			data = data.data;
		} else if (data.hasOwnProperty('error')) {
			err = data.error;
			data = null;

			if (err.code == 202 && mmm_loggedIn) {
				mmm_logged_out();
				return mmm_notice('Your MaMoMa session has expired; please log in again!');
			}
		} else {
			err = me1('The MaMoMa server returned an invalid JSON response');
		}
		callback(data, err);
	}

	return mmm_wrap_err('cb_mmm_generic', function() {
		var data = null;
		if ($.type(resp) === "string") {
			data = null;
			try {
				data = JSON.parse(resp);
			} catch (e) {
				err = me1('The MaMoMa server returned an invalid JSON response (' + e + ')');
			}
			return process(data, err);
		} else if (resp.hasOwnProperty('data') || resp.hasOwnProperty('error')) {
			return process(resp, null);
		} else {
			err = me1('The request to the MaMoMa server failed');
		}
	});
}

function mmm_change_id(node, repl) {
	// Okay, so some of those are jQuery objects, some are... just objects!
	var id;
	if ($.type(node.attr) === "function")
		id = node.attr('id');
	else if (node.id != null)
		id = node.id;

	if (id == null)
		return;
	var pos = id.indexOf('--');
	if (pos == -1)
		return;
	id = id.substr(0, pos + 2) + repl;

	if ($.type(node.attr) === "function")
		node.attr('id', id);
	else
		node.id = id;
}

function mmm_change_id_rec(node, repl) {
	mmm_change_id(node, repl);
	if (node.children == null)
		return;
	if ($.type(node.children) === "function")
		node.children().each(function(idx, child) { mmm_change_id_rec(child, repl); });
	else
		for (var i = 0; i < node.children.length; i++)
			mmm_change_id_rec(node.children[i]);
}

function highlight_node(n, highlight, title) {
	if (highlight) {
		n.css('font-weight', 'bold');
		n.css('color', 'red');
	} else {
		n.css('font-weight', 'normal');
		n.css('font-size', '100%');
		n.css('color', 'black');
	}
}

function format_stats(id, title, value, avail) {
	var highlight = false;
	var n = $(id);

	if (avail > 0) {
		var pct = parseFloat(Math.floor(value * 10000 / avail) / 100).toFixed(2);
		highlight = pct >= highlight_threshold;
		if (title != '')
			n.text(title + value + ' / ' + avail + ' (' + pct + '%)');
		else
			n.text('' + pct + '%');
	} else {
		n.text('-');
	}

	highlight_node(n, highlight, title);
}

function cb_mmm_list_domains(data, err) {
	return mmm_wrap_err('cb_mmm_list_domains', function() {
		if (err !== null) {
			mmm_error('mmm-domains-err-msg',
			    'MaMoMa domains list error: ' + err.message);
			return;
		}

		var avail = data.available_mb;
		data = data.domains;
		var template = $('#mmm-domains--template');
		$('#mmm-domains-label').text('Available domains:');
		var dlen = data.length;
		var rowcount = (dlen + 2 - (dlen % 2)) / 2;
		for (var i = 0; i < rowcount; i++)
		{
			var rowidx = i + 1;
			var nr = template.clone();
			mmm_change_id_rec(nr, rowidx);
			template.before(nr);
			nr.show();
		}
		var total_quota = 0, total_used = 0;
		for (var i = 0; i < data.length; i++)
		{
			var rowidx = (i % rowcount) + 1;
			var prefix = '#mmm-domains-';
			if (i < rowcount) {
				prefix += 'left';
			} else {
				prefix += 'right';
			}

			var n = $(prefix + '--' + rowidx);
			mmm_change_id_rec(n, data[i].name);
			n.find('a').each(function(idx, a) {
				a.innerHTML = data[i].name;
			});
			n.find('span').each(function(idx, a) {
				if (data[i].description != "")
					a.innerHTML = ' (' + data[i].description + ')';
				else
					a.innerHTML = '';
			});
			n.show();

			var nearq = $(prefix + '-nearq--' + rowidx);
			nearq.text(data[i].highlighted ? '!' : '-');
			highlight_node(nearq, data[i].highlighted, 'nonempty');
			nearq.show();

			$(prefix + '-quota--' + rowidx).text(data[i].quota_mb);
			$(prefix + '-quota--' + rowidx).show();
			total_quota += data[i].quota_mb;
			$(prefix + '-used--' + rowidx).text(data[i].used_mb);
			$(prefix + '-used--' + rowidx).show();
			total_used += data[i].used_mb;
			$(prefix + '-usage--' + rowidx).text(7);
			$(prefix + '-usage--' + rowidx).show();
			format_stats(prefix + '-usage--' + rowidx, '', data[i].used_mb, data[i].quota_mb);
		}
		format_stats('#mmm-domains-stats-quota', 'Total quota assigned (MB): ', total_quota, avail);
		format_stats('#mmm-domains-stats-used', 'Total space used (MB): ', total_used, avail);
		$('#mmm-domains-table').find('a').click(function() { mmm_domain_choose($(this)); });
	});
}

function uncode(s) {
	var res = '';
	for (var i = 0, len = s.length; i < len; i++) {
		var ch = s[i];
		switch (ch) {
			case '_':
				res += '_u';
				break;

			case '.':
				res += '_d';
				break;
			case '@':
				res += '_a';
				break;

			default:
				res += ch;
				break;
		}
	}
	return res;
}

function duncode(s) {
	var res = '';
	for (var i = 0, len = s.length; i < len; i++) {
		var ch = s[i];
		if (ch != '_') {
			res += ch;
			continue;
		}
		ch = s[++i];
		switch (ch) {
			case 'u':
				res += '_';
				break;

			case 'd':
				res += '.';
				break;

			case 'a':
				res += '@';
				break;

			default:
				// oops...?
				res += ch;
				break;
		}
	}
	return res;
}

function mmm_display_users() {
	var template = $('#mmm-users--template');
	$('#mmm-users-label').text('Available users in domain ' + mmm_domain + ':');
	for (var username in mmm_users) {
		if (!mmm_users.hasOwnProperty(username))
			continue;

		var n = template.clone();
		var iusername = uncode(username);
		mmm_change_id_rec(n, iusername);
		n.find('a.mmm-users-a-username').each(function(idx, a) {
			/**
			 * Uh, I really need to escape this, but
			 * $.text() goes infinitely recursive on
			 * me here
			 */
			a.innerHTML = username;
		});

		var u = mmm_users[username];
		var pat = /^mmm-users-(.*)--/;
		n.find('td').each(function(idx, td) {
			var id;
			if ($.type(td.attr) === "function")
				id = td.attr('id');
			else
				id = td.id;
			if (id == null)
				return;
			var m = id.match(pat);
			if (m == null)
				return;
			var val = u[m[1]];
			if (val == null)
				return;
			/* FIXME: escape this! */
			td.innerHTML = val;
		});

		template.before(n);
		format_stats('#mmm-users-usage--' + iusername, '',
		    u['used_mb'], u['quota_mb']);
		n.show();
	}
	$('#mmm-users-table').find('a.mmm-users-a-username').click(function() { mmm_user_choose($(this)); });
	$('#mmm-users-table').find('a.mmm-users-a-remove').click(function() { mmm_user_delete($(this)); });

	mmm_top_only('users');
}

function mmm_display_aliases() {
	var template = $('#mmm-aliases--template');
	$('#mmm-aliases-label').text('Available aliases in domain ' + mmm_domain + ':');
	for (var aliasname in mmm_aliases) {
		if (!mmm_aliases.hasOwnProperty(aliasname))
			continue;

		var n = template.clone();
		mmm_change_id_rec(n, aliasname);
		var alias_data = mmm_aliases[aliasname];
		var aliasplit = aliasname.split('--');
		var alias_id = aliasplit.shift();
		var alias_source = aliasplit.join('--');
		n.find('a.mmm-aliases-a-source').each(function(idx, a) {
			/**
			 * Uh, I really need to escape this, but
			 * $.text() goes infinitely recursive on
			 * me here
			 */
			a.innerHTML = alias_source;
		});

		var pat = /^mmm-aliases-(.*?)--/;
		n.find('td').each(function(idx, td) {
			var id;
			if ($.type(td.attr) === "function")
				id = td.attr('id');
			else
				id = td.id;
			if (id == null)
				return;
			var m = id.match(pat);
			if (m == null || m[1] == 'source')
				return;
			var val = alias_data[m[1]];
			if (val == null)
				return;
			/* FIXME: escape this! */
			td.innerHTML = val;
		});

		template.before(n);
		n.show();
	}
	$('#mmm-aliases-table').find('a.mmm-aliases-a-source').click(function() { mmm_alias_choose($(this)); });
	$('#mmm-aliases-table').find('a.mmm-aliases-a-remove').click(function() { mmm_alias_delete($(this)); });

	mmm_top_only('aliases');
}

function cb_mmm_list_users(data, err) {
	return mmm_wrap_err('cb_mmm_list_users', function() {
		if (err !== null) {
			mmm_error('mmm-users-err-msg',
			    'MaMoMa users list error: ' + err.message);
			return;
		}

		mmm_users = {};
		for (var i = 0; i < data.length; i++)
		{
			var username = data[i].email.split('@', 1)[0];
			mmm_users[username] = data[i];
		}
		mmm_display_users();
	});
}

function cb_mmm_list_aliases(data, err) {
	return mmm_wrap_err('cb_mmm_list_aliases', function() {
		if (err !== null) {
			mmm_error('mmm-aliases-err-msg',
			    'MaMoMa aliases list error: ' + err.message);
			return;
		}

		mmm_aliases = {};
		for (var i = 0; i < data.length; i++)
		{
			var aliasname = [data[i].id, data[i].source.split('@', 1)[0]].join('--');
			mmm_aliases[aliasname] = data[i];
		}
		mmm_display_aliases();
	});
}

function cb_mmm_domain_update(data, err) {
	return mmm_wrap_err('cb_mmm_domain_update', function() {
		if (err !== null) {
			mmm_error('mmm-domain-err-msg',
			    'MaMoMa domain update error: ' + err.message);
			return;
		}

		$('#mmm-domain-err-msg').hide();
		$('#mmm-domain-err-msg').text('');
		mmm_list_domains();
	});
}

function cb_mmm_user_update(data, err) {
	return mmm_wrap_err('cb_mmm_user_update', function() {
		if (err !== null) {
			mmm_error('mmm-user-err-msg',
			    'MaMoMa user update error: ' + err.message);
			return;
		}

		$('#mmm-user-err-msg').hide();
		$('#mmm-user-err-msg').text('');
		mmm_list_users(mmm_domain);
	});
}

function cb_mmm_alias_update(data, err) {
	return mmm_wrap_err('cb_mmm_alias_update', function() {
		if (err !== null) {
			mmm_error('mmm-alias-err-msg',
			    'MaMoMa alias update error: ' + err.message);
			return;
		}

		$('#mmm-alias-err-msg').hide();
		$('#mmm-alias-err-msg').text('');
		mmm_list_aliases(mmm_domain);
	});
}

function mmm_element_domain(node, up)
{
	while (up > 0) {
		node = node.parent();
		up--;
	}
	var id = node.attr('id');
	if (id == null)
		return null;
	var pos = id.indexOf('--');
	if (pos == -1)
		return null;
	return id.substr(pos + 2);
}

function mmm_domain_choose(a) {
	return mmm_wrap_ui('mmm_domain_choose', function() {
		var domain = mmm_element_domain(a, 1);
		if (domain == null)
			return;
		mmm_domain = domain;
		mmm_list_users(domain);
	});
}

function mmm_user_choose(a) {
	return mmm_wrap_ui('mmm_user_choose', function() {
		var username = mmm_element_domain(a, 2);
		if (username == null)
			return;
		mmm_user_edit(duncode(username));
	});
}

function mmm_alias_choose(a) {
	return mmm_wrap_ui('mmm_alias_choose', function() {
		var aliasname = mmm_element_domain(a, 2);
		if (aliasname == null)
			return;
		mmm_alias_edit(aliasname);
	});
}

function cb_mmm_user_delete(data, err) {
	return mmm_wrap_err('cb_mmm_user_delete', function() {
		if (err) {
			mmm_error('mmm-users-err-msg',
			    'Could not delete the e-mail user account: ' + err.message);
			return;
		}

		$('#mmm-users-err-msg').hide();
		$('#mmm-users-err-msg').text('');
		mmm_list_users(mmm_domain);
	});
}

function cb_mmm_alias_delete(data, err) {
	return mmm_wrap_err('cb_mmm_alias_delete', function() {
		if (err) {
			mmm_error('mmm-aliases-err-msg',
			    'Could not delete the e-mail alias account: ' + err.message);
			return;
		}

		$('#mmm-aliases-err-msg').hide();
		$('#mmm-aliases-err-msg').text('');
		mmm_list_aliases(mmm_domain);
	});
}

/**
 * Function:
 *	cb_mmm_confirm_user_delete	- send a MaMoMa API "delete user" request
 * Inputs:
 *	username		- the username selected by the user for removal
 *	resp			- the Bootbox dialog response; really delete?
 * Returns:
 *	Nothing.
 * Modifies:
 *	Unsets mmm_inDialog.
 *	Sends a UserDelete request for the user@domain selected by the user
 *	for removal.
 */
function cb_mmm_confirm_user_delete(username, resp) {
	return mmm_wrap_err('cb_mmm_confirm_user_delete', function() {
		mmm_inDialog = false;

		if (!resp)
			return;
		mmm_post('UserDelete', { email: username + '@' + mmm_domain },
		    cb_mmm_user_delete);
	});
}

function cb_mmm_confirm_alias_delete(alias_data, resp) {
	return mmm_wrap_err('cb_mmm_confirm_alias_delete', function() {
		mmm_inDialog = false;

		if (!resp)
			return;
		mmm_post('AliasDelete', { source: alias_data.source, id: alias_data.id },
		    cb_mmm_alias_delete);
	});
}

function mmm_user_delete(rm) {
	return mmm_wrap_ui('mmm_user_delete', function() {
		var username = mmm_element_domain(rm, 2);
		if (username == null)
			return;
		var iusername = duncode(username);
		mmm_inDialog = true;
		bootbox.confirm("Are you sure you want to delete the " +
		    iusername + "@" + mmm_domain + " e-mail user account?",
		    function(resp) {
			cb_mmm_confirm_user_delete(iusername, resp); 
		});
	});
}

function mmm_alias_delete(rm) {
	return mmm_wrap_ui('mmm_alias_delete', function() {
		var aliasname = mmm_element_domain(rm, 2);
		if (aliasname == null)
			return;
		var alias_data = mmm_aliases[aliasname];
		if (alias_data == null)
			return;
		mmm_inDialog = true;
		bootbox.confirm("Are you sure you want to delete the " +
		    alias_data.source + " e-mail alias?",
		    function(resp) {
			cb_mmm_confirm_alias_delete(alias_data, resp); 
		});
	});
}

function mmm_user_edit(username) {
	mmm_user = username;
	$('#mmm-user-label').text(mmm_user + '@' + mmm_domain);
	$('#mmm-user-err-msg').text('');
	$('#mmm-user-at-domain').hide();

	var u = mmm_users[username];
	$('input[id^="mmm-user-"]').each(function(idx, field) {
		var id;
		if ($.type(field.attr) === "function")
			id = field.attr('id');
		else
			id = field.id;
		if (id == null)
			return;
		id = id.substr(9);
		if (id == "username" || id == "used_mb") {
			field.readOnly = true;
		} else if (id == "home" || id == "uid" || id == "gid") {
			field.readOnly = true;
			$('#mmm-group-user-' + id).show();
		}
		if (id == 'username')
			field.value = username;
		else if (u == null || u[id] == null)
			field.value = '';
		else if (id == 'active')
			field.checked = u[id] == "Y";
		else
			field.value = u[id];
	});

	$('mmm-group-user-used_mb').show();

	mmm_top_only('user');
}

function mmm_alias_edit(aliasname) {
	mmm_alias = aliasname;
	var aliasplit = aliasname.split('--');
	mmm_alias_id = aliasplit.shift();
	mmm_alias_source = aliasplit.join('--') + '@' + mmm_domain;
	$('#mmm-alias-label').text(mmm_alias_source);
	$('#mmm-alias-err-msg').text('');
	$('#mmm-alias-at-domain').hide();

	var u = mmm_aliases[aliasname];
	$('input[id^="mmm-alias-"]').each(function(idx, field) {
		var id;
		if ($.type(field.attr) === "function")
			id = field.attr('id');
		else
			id = field.id;
		if (id == null)
			return;
		id = id.substr(10);
		if (id == "source")
			field.readOnly = true;
		if (id == 'source')
			field.value = mmm_alias_source;
		else if (u == null || u[id] == null)
			field.value = '';
		else if (id == 'active')
			field.checked = u[id] == "Y";
		else
			field.value = u[id];
	});

	mmm_top_only('alias');
}

function mmm_domain_new() {
	return mmm_wrap_ui('mmm_domain_new', function() {
		mmm_domain = null;
		$('#mmm-domain-label').text('Create a new domain');
		$('#mmm-domain-err-msg').text('');

		$('input[id^="mmm-domain-"]').each(function(idx, field) {
			var id;
			if ($.type(field.attr) === "function")
				id = field.attr('id');
			else
				id = field.id;
			if (id == null)
				return;
			id = id.substr(11);
			if (id == "username")
				field.readOnly = false;
			else
				field.value = '';
		});

		mmm_top_only('domain');
	});
}

function mmm_user_new() {
	return mmm_wrap_ui('mmm_user_new', function() {
		mmm_user = null;
		$('#mmm-user-label').text('Create a new account @' + mmm_domain);
		$('#mmm-user-err-msg').text('');
		$('#mmm-user-at-domain').text('@' + mmm_domain);
		$('#mmm-user-at-domain').show();

		$('input[id^="mmm-user-"]').each(function(idx, field) {
			var id;
			if ($.type(field.attr) === "function")
				id = field.attr('id');
			else
				id = field.id;
			if (id == null)
				return;
			id = id.substr(9);
			if (id == "username")
				field.readOnly = false;
			else if (id == "home" || id == "uid" || id == "gid")
				$('#mmm-group-user-' + id).hide();
			if (id == 'active')
				field.checked = true;
			else
				field.value = '';
		});

		$('mmm-group-user-used_mb').hide();

		mmm_top_only('user');
	});
}

function mmm_alias_new() {
	return mmm_wrap_ui('mmm_alias_new', function() {
		mmm_alias = null;
		$('#mmm-alias-label').text('Create a new alias @' + mmm_domain);
		$('#mmm-alias-err-msg').text('');
		$('#mmm-alias-at-domain').text('@' + mmm_domain);
		$('#mmm-alias-at-domain').show();

		$('input[id^="mmm-alias-"]').each(function(idx, field) {
			var id;
			if ($.type(field.attr) === "function")
				id = field.attr('id');
			else
				id = field.id;
			if (id == null)
				return;
			id = id.substr(10);
			if (id == "source")
				field.readOnly = false;
			if (id == 'active')
				field.checked = true;
			else
				field.value = '';
		});

		mmm_top_only('alias');
	});
}

/**
 * Function:
 *	mmm_domain_update		- send a MaMoMa API "update domain" request
 * Inputs:
 *	None.
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a DomainCreate request.
 */
function mmm_domain_update() {
	return mmm_wrap_ui('mmm_domain_update', function() {
		var d;
		if (mmm_domain == null) {
			var name = $('#mmm-domain-name').val();
			if (name == null || name == '') {
				mmm_error('mmm-domain-err-msg', 'No domain name specified');
				return false;
			}
			d = {
				'name': name,
			};
		} else {
			mmm_error('mmm-domain-err-msg',
			    'Internal error: how did we ever get to domain updates? (mmm_domain is ' + mmm_domain + ')');
			return false;
		}

		/* Pfth, simpler than in other requests. */
		mmm_post('DomainCreate', d, cb_mmm_domain_update);
	});
}

/**
 * Function:
 *	mmm_user_update		- send a MaMoMa API "update user" request
 * Inputs:
 *	None.
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a UserCreate or UserUpdate request for
 *	the user@domain selected (or entered) by the user.
 */
function mmm_user_update() {
	return mmm_wrap_ui('mmm_user_update', function() {
		var u;
		if (mmm_user == null) {
			var username = $('#mmm-user-username').val();
			if (username == null || username == '') {
				mmm_error('mmm-user-err-msg', 'No username specified');
				return false;
			}
			u = {
				'email': username + '@' + mmm_domain,
				'active': null,
				'quota_mb': null,
				'home': null,
				'uid': null,
				'gid': null,
				'password': null,
			};
		} else {
			if (mmm_users == null || mmm_users[mmm_user] == null)
				return false;
			u = mmm_users[mmm_user];
			if (u == null) {
				mmm_err('mmm-user-err-msg',
				    'Internal error: internal user list inconsistency');
				return false;
			}
		}

		var uu = {};
		for (var key in u) {
			if (!u.hasOwnProperty(key))
				continue;
			if (key == 'email') {
				uu[key] = u[key];
				continue;
			}
			var field = $('#mmm-user-' + key);
			if (field.length != 1)
				continue;
			field = field[0];
			if (!field)
				continue;
			var val;
			if (key == 'active')
				val = field.checked? 'Y': 'N';
			else
				val = field.value;
			uu[key] = val;
		}
		if (mmm_user == null)
			uu['home'] = uu['uid'] = uu['gid'] = '';

		var pw = $('#mmm-user-password').val();
		var conf = $('#mmm-user-confirm').val();
		var pwempty = (pw == null || pw == '');
		var confempty = (conf == null || conf == '');
		if (pwempty && !confempty || !pwempty && confempty) {
			mmm_error('mmm-user-err-msg',
			    'Password not confirmed');
			return false;
		} else if (pwempty && mmm_user == null) {
			mmm_error('mmm-user-err-msg',
			    'No password specified for a new account');
			return false;
		} else if (!pwempty) {
			if (pw != conf) {
				mmm_error('mmm-user-err-msg',
				    'Passwords do not match');
				return false;
			}
			uu['password'] = pw;
		}

		var action = mmm_user != null? 'Update': 'Create';
		mmm_post('User' + action, uu, cb_mmm_user_update);
	});
}

/**
 * Function:
 *	mmm_alias_update	- send a MaMoMa API "update alias" request
 * Inputs:
 *	None.
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a AliasCreate or AliasUpdate request for
 *	the alias@domain selected (or entered) by the user.
 */
function mmm_alias_update() {
	return mmm_wrap_ui('mmm_alias_update', function() {
		var u;
		if (mmm_alias == null) {
			var aliasname = $('#mmm-alias-source').val();
			if (aliasname == null || aliasname == '') {
				mmm_error('mmm-alias-err-msg', 'No alias name specified');
				return false;
			}
			u = {
				'source': aliasname + '@' + mmm_domain,
				'active': null,
				'destination': null,
			};
		} else {
			if (mmm_aliases == null || mmm_aliases[mmm_alias] == null)
				return false;
			u = mmm_aliases[mmm_alias];
			if (u == null) {
				mmm_err('mmm-alias-err-msg',
				    'Internal error: internal alias list inconsistency');
				return false;
			}
		}

		var uu = {};
		for (var key in u) {
			if (!u.hasOwnProperty(key))
				continue;
			if (key == 'source' || key == 'id') {
				uu[key] = u[key];
				continue;
			}
			var field = $('#mmm-alias-' + key);
			if (field.length != 1)
				continue;
			field = field[0];
			if (!field)
				continue;
			var val;
			if (key == 'active')
				val = field.checked? 'Y': 'N';
			else
				val = field.value;
			uu[key] = val;
		}

		var action = mmm_alias != null? 'Update': 'Create';
		mmm_post('Alias' + action, uu, cb_mmm_alias_update);
	});
}

function mmm_show_login() {
	return mmm_wrap_err('mmm_show_login', function() {
		$('#mmmFormLoginErrMsg').text('');
		$('#mmmFormLoginErrMsg').hide();
		mmm_top_only('login');
	});
}

function cb_mmm_restoresession(data, err) {
	return mmm_wrap_err('cb_mmm_restoresession', function() {
		if (data !== null) {
			mmm_logged_in(data);
			return;
		}

		mmm_show_login();
		if (err !== null && err.code != 202)
			mmm_error('mmmFormLoginErrMsg',
			    'The MaMoMa server could not complete the session request');
	});
}

/**
 * Function:
 *	mmm_restoresession	- send a MaMoMa API "restore session" request
 * Inputs:
 *	None.
 * Returns:
 *	Nothing.
 * Modifies:
 *	Nothing by itself; sends a MMMRestoreSession request, hopefully using
 *	the browser cookie stored at last login time.
 */
function mmm_restoresession() {
	return mmm_wrap_err('mmm_restoresession', function() {
		if (mmm_inDialog)
			return false;

		mmm_get('MMMRestoreSession', {}, cb_mmm_restoresession);
	});
}

function mmm_startup() {
	mmm_top_only(null);
	mmm_restoresession();
}
