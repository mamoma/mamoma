<?php
/*-
 * Copyright (c) 2014, 2016, 2017, 2022  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
declare(strict_types=1);

require('../lib/mmm-core.php');

function mamoma_err(int $code, string $msg): string
{
	return json_encode(array('error' => array('code' => $code, 'message' => $msg)));
}

function mamoma_mmm_err(): string
{
	global $mmm_err_code, $mmm_err;
	return mamoma_err($mmm_err_code, isset($mmm_err) ? $mmm_err : '<no error message>');
}

function mamoma_get_auth_bearer(): ?string
{
	$auth_header = mmm_aget(getallheaders(), 'Authorization');
	if (!isset($auth_header)) {
		return null;
	}
	$auth_fields = explode(' ', $auth_header);
	if (count($auth_fields) != 2 || $auth_fields[0] != 'Bearer') {
		return null;
	}
	$auth_value = base64_decode($auth_fields[1], true);
	if ($auth_value === false) {
		return null;
	}
	return $auth_value;
}

function validate_int(string $name, ?string $value, int $min_val, int $max_val): int {
	if (!isset($value)) {
		print mamoma_err(MMM_ERR_USAGE, "No $name specified");
		exit(0);
	}
	if (!is_numeric($value)) {
		print mamoma_err(MMM_ERR_USAGE, "Not a numeric $name value: $value");
		exit(0);
	}
	$intval = intval($value);
	if ($intval < $min_val || $intval > $max_val) {
		print mamoma_err(MMM_ERR_USAGE, "The $name value $value is outside the $min_val-$max_val range");
		exit(0);
	}
	return $intval;
}

try {
	header("Content-Type: application/json");

	$err = null;
	$data = null;

	$mmm = mmm_aget($_GET, 'mmm');
	if (!isset($mmm) && !mmm_empty(mmm_aget($_SERVER, 'PATH_INFO'))) {
		$pathinfo = explode('/', $_SERVER['PATH_INFO']);
		if ($pathinfo[0] == "")
			array_shift($pathinfo);
		if (count($pathinfo) > 1) {
			print mamoma_err(MMM_ERR_INTERNAL, 'FIXME: pathinfo parsing not complete yet :)');
			exit(0);
		}
		$mmm = $pathinfo[0];
	}

	$sid = isset($_GET['sid'])? $_GET['sid']: mmm_aget($_COOKIE, 'MMM_SessionID');
	if (!isset($sid)) {
		$sid = mamoma_get_auth_bearer();
	}
	if (isset($mmm) && $mmm != 'MMMLogin' && $mmm != 'MMMVersion' && $mmm != 'MMMFeatures') {
		if (!isset($sid)) {
			print mamoma_err(MMM_ERR_INVALID_SID, 'Session ID (sid) not specified');
			exit(0);
		}
		$sess = MMM_MMMRestoreSession($sid);
		if (!isset($sess)) {
			print mamoma_mmm_err();
			exit(0);
		}
		setcookie('MMM_SessionID', $sid, array('expires' => time() + 3600));
	}

	switch ($mmm) {
		case 'AliasesList':
			$data = MMM_AliasesList($sid, mmm_aget($_GET, 'domain'));
			break;

		case 'AliasCreate':
			$data = MMM_AliasCreate($sid, array(
			    'source' => mmm_aget($_POST, 'source'),
			    'active' => mmm_aget($_POST, 'active'),
			    'destination' => mmm_aget($_POST, 'destination'),
			));
			break;

		case 'AliasDelete':
			$data = MMM_AliasDelete($sid, array(
			    'source' => mmm_aget($_POST, 'source'),
			    'id' => mmm_aget($_POST, 'id'),
			));
			break;

		case 'AliasUpdate':
			$data = MMM_AliasUpdate($sid, array(
			    'source' => mmm_aget($_POST, 'source'),
			    'id' => mmm_aget($_POST, 'id'),
			    'active' => mmm_aget($_POST, 'active'),
			    'destination' => mmm_aget($_POST, 'destination'),
			));
			break;

		case 'DomainsList':
			$highlight = mmm_aget($_GET, 'highlight');
			$highlight_threshold = mmm_aget($_GET, 'highlight_threshold');
			if (!isset($highlight)) {
				if (isset($highlight_threshold)) {
					print mamoma_err(MMM_ERR_USAGE, 'No highlight, but highlight_threshold specified');
					exit(0);
				}
				$data = MMM_DomainsList($sid, null, null);
			} elseif ($highlight == 'quota') {
				$highlight_threshold = validate_int('highlight_threshold', $highlight_threshold, 0, 100);
				$data = MMM_DomainsList($sid, $highlight, $highlight_threshold);
			} else {
				print mamoma_err(MMM_ERR_USAGE, 'The only supported highlight type is "quota"');
				exit(0);
			}
			break;

		case 'DomainCreate':
			$data = MMM_DomainCreate($sid, array(
			    'name' => mmm_aget($_POST, 'name'),
			));
			break;

		case 'MMMFeatures':
			$data = MMM_MMMFeatures();
			break;

		case 'MMMLogin':
			$username = mmm_aget($_POST, 'username');
			$password = mmm_aget($_POST, 'password');
			if (!isset($username) || !isset($password))
				$err = mamoma_err(MMM_ERR_LOGIN, 'No username or password specified');
			else
				$data = MMM_MMMLogin($username, $password);
			if (isset($data) && isset($data['sid']))
				setcookie('MMM_SessionID', $data['sid'], array('expires' => time() + 3600));
			break;

		case 'MMMLogout':
			$data = MMM_MMMLogout($sid);
			break;

		case 'MMMRestoreSession':
			$data = $sess;
			if (isset($data) && isset($data['sid']))
				setcookie('MMM_SessionID', $data['sid'], array('expires' => time() + 3600));
			break;

		case 'MMMVersion':
			$data = MMM_MMMVersion();
			break;

		case 'UserCreate':
			$data = MMM_UserCreate($sid, array(
			    'email' => mmm_aget($_POST, 'email'),
			    'active' => mmm_aget($_POST, 'active'),
			    'quota_mb' => mmm_aget($_POST, 'quota_mb'),
			    'home' => mmm_aget($_POST, 'home'),
			    'uid' => mmm_aget($_POST, 'uid'),
			    'gid' => mmm_aget($_POST, 'gid'),
			    'password' => mmm_aget($_POST, 'password'),
			));
			break;

		case 'UserDelete':
			$data = MMM_UserDelete($sid, mmm_aget($_POST, 'email'));
			break;

		case 'UserUpdate':
			$data = MMM_UserUpdate($sid, array(
			    'email' => mmm_aget($_POST, 'email'),
			    'active' => mmm_aget($_POST, 'active'),
			    'quota_mb' => mmm_aget($_POST, 'quota_mb'),
			    'home' => mmm_aget($_POST, 'home'),
			    'uid' => mmm_aget($_POST, 'uid'),
			    'gid' => mmm_aget($_POST, 'gid'),
			    'password' => mmm_aget($_POST, 'password'),
			));
			break;

		case 'UsersList':
			$data = MMM_UsersList($sid, mmm_aget($_GET, 'domain'));
			break;

		default:
			$err = mamoma_err(MMM_ERR_USAGE, 'Unrecognized MaMoMa query');
			break;
	}

	if (isset($err))
		print $err;
	else if ($data === null)
		print mamoma_mmm_err();
	else
		print json_encode(array('data' => $data));
} catch (Exception $e) {
	try {
		error_log("mamoma: caught an exception: ".$e->getMessage());
		print mamoma_err(MMM_ERR_INTERNAL, $e->getMessage());
	} catch (Exception $e) {
		// BAH!
	}
}
?>
