<?php
/*-
 * Copyright (c) 2014 - 2017, 2022  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
declare(strict_types=1);

/** @var integer $mmm_err_code The numeric code of the last error encountered, or 0 if none. */
$mmm_err_code = 0;
/** @var ?string $mmm_err The description of the last error encountered, or null if none. */
$mmm_err = null;

define('MMMVersion', '2.0.0.b6');

define('MMM_ERR_NONE', 0);

define('MMM_ERR_DB', 100);

define('MMM_ERR_USAGE', 200);
define('MMM_ERR_LOGIN', 201);
define('MMM_ERR_INVALID_SID', 202);

define('MMM_ERR_EXISTS', 300);

define('MMM_ERR_NOT_ADMIN', 400);

define('MMM_ERR_INTERNAL', 999);

function mmm_err(int $code, string $msg): void
{
	global $mmm_err, $mmm_err_code;

	$mmm_err_code = $code;
	$mmm_err = $msg;
}

function mmm_dberr(PDOStatement $st, string $msg): void
{
	mmm_err(MMM_ERR_DB, $msg.': '.($st->errorInfo()[2]));
}

/** @var ?array $mmm_conf The contents of the mmm.conf file. */
$mmm_conf = null;

function mmm_conf(): array
{
	global $mmm_conf;
	if (isset($mmm_conf))
		return $mmm_conf;

	$conf = parse_ini_file('../conf/mmm.conf', true);
	if (!$conf)
		$mmm_conf = array();
	else
		$mmm_conf = $conf;
	return $mmm_conf;
}

function mmm_dbprepare(PDO $conn): ?array
{
	$d = array('conn' => $conn, 'st' => array());

	$st = array(
		'access_user_domain_id' => '
SELECT COUNT(*)
FROM mmm_user_domains
WHERE user_id = ? AND domain_id = ?
',
		'access_user_domain_name' => '
SELECT COUNT(*)
FROM mmm_user_domains ud
    INNER JOIN virtual_domains d ON d.id = ud.domain_id
WHERE ud.user_id = ? AND d.name = ?
',

		'alias_delete' => '
DELETE FROM virtual_aliases
WHERE id = ?
',
		'alias_get' => '
SELECT id, domain_id, source
FROM virtual_aliases
WHERE source = ? AND id = ?
',
		'alias_get_by_destination' => '
SELECT id, domain_id, source
FROM virtual_aliases
WHERE source = ? AND destination = ?
',
		'alias_insert' => '
INSERT INTO virtual_aliases(domain_id, source, active, destination)
VALUES (?, ?, ?, ?)
',
		'alias_update' => '
UPDATE virtual_aliases
SET active = ?, destination = ?
WHERE id = ?
',

		'dom_get_by_name' => '
SELECT id, name
FROM virtual_domains
WHERE name = ?
',
		'dom_insert' => '
INSERT INTO virtual_domains(name)
VALUES (?)
',
		'dom_list_all_h_none' => '
SELECT d.name, d.description,
    FLOOR(SUM(CASE WHEN u.quota_kb IS NULL THEN 0 ELSE u.quota_kb END) / 1024) as quota_mb,
    FLOOR(SUM(CASE WHEN u.used_kb IS NULL THEN 0 ELSE u.used_kb END) / 1024) as used_mb,
    "N" as highlighted
FROM virtual_domains d
    LEFT JOIN virtual_users u ON u.domain_id = d.id
GROUP BY d.name
ORDER BY d.name
',
		'dom_list_all_h_quota' => '
SELECT d.name, d.description,
    FLOOR(SUM(CASE WHEN u.quota_kb IS NULL THEN 0 ELSE u.quota_kb END) / 1024) as quota_mb,
    FLOOR(SUM(CASE WHEN u.used_kb IS NULL THEN 0 ELSE u.used_kb END) / 1024) as used_mb,
    CASE WHEN COUNT(
        CASE WHEN u.quota_kb IS NULL
        THEN NULL
        ELSE (
            CASE WHEN u.quota_kb * ? < (
                CASE WHEN u.used_kb IS NULL
                THEN 0
                ELSE u.used_kb
                END
            ) * 100
            THEN 1
            ELSE NULL END
        )
        END
    ) > 0
    THEN "Y"
    ELSE "N"
    END AS highlighted
FROM virtual_domains d
    LEFT JOIN virtual_users u ON u.domain_id = d.id
GROUP BY d.name
ORDER BY d.name
',
		'dom_list_allowed' => '
SELECT d.name
FROM mmm_user_domains ud
    INNER JOIN virtual_domains d ON d.id = ud.domain_id
WHERE ud.user_id = ?
ORDER BY d.name
',
		'dom_list_aliases' => '
SELECT a.id, a.source, a.active, a.destination
FROM virtual_aliases a
    INNER JOIN virtual_domains d ON a.domain_id = d.id
WHERE d.name = ?
ORDER BY a.source
',
		'dom_list_users' => '
SELECT u.email, u.active, u.quota_kb, u.used_kb, u.home, u.uid, u.gid
FROM virtual_users u
    INNER JOIN virtual_domains d ON u.domain_id = d.id
WHERE d.name = ?
ORDER BY u.email
',

		'login_session_ins' => '
INSERT INTO mmm_sessions(id, user_id, last)
VALUES (?, ?, NOW())
',
		'login_session_q' => '
SELECT id, user_id, last
FROM mmm_sessions
WHERE id = ?
',
		'login_session_update' => '
UPDATE mmm_sessions
SET last = NOW()
WHERE id = ?
',

		'login_userid_q' => '
SELECT id, name, username, password, admin, active
FROM mmm_users
WHERE id = ?
',
		'login_username_q' => '
SELECT id, name, username, password, admin, active
FROM mmm_users
WHERE username = ?
',

		'logout_session_del' => '
DELETE FROM mmm_sessions
WHERE id = ?
',

		 'user_delete' => '
DELETE FROM virtual_users
WHERE id = ?
',
		 'user_get' => '
SELECT id, domain_id, email
FROM virtual_users
WHERE email = ?
',
		 'user_insert' => '
INSERT INTO virtual_users(domain_id, email, active, quota_kb, home, uid, gid, password)
VALUES (?, ?, ?, ?, ?, ?, ?, ?)
',
		 'user_update' => '
UPDATE virtual_users
SET active = ?, quota_kb = ?, home = ?, uid = ?, gid = ?
WHERE id = ?
',
		 'user_update_pw' => '
UPDATE virtual_users
SET active = ?, quota_kb = ?, home = ?, uid = ?, gid = ?, password = ?
WHERE id = ?
',
	);

	foreach ($st as $id => $q) {
		try {
			$prep = $conn->prepare($q);
			$prep->setFetchMode(PDO::FETCH_ASSOC);
			$d['st'][$id] = $prep;
		} catch (PDOException $e) {
			return mmm_err(MMM_ERR_DB, "Could not prepare the $id query: ".$conn->errorInfo()[2]);
		}
	}
	return $d;
}

function mmm_dbconn(): ?array
{
	try {
		$conf = mmm_conf();
		$conn = new PDO($conf['db']['db'],
		    $conf['db']['username'], $conf['db']['password'],
		    array(PDO::ATTR_PERSISTENT => boolval(mmm_aget($conf['db'], 'persistent'))));
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}

	$conn = mmm_dbprepare($conn);
	if (!$conn)
		return $conn;
	global $mmm_dbconn;
	$mmm_dbconn = $conn;
	return $conn;
}

// The value can be either a string or an integer.
// We can't use union types before PHP 8.0.
function mmm_user_info(array $conn, string $type, $value): ?array
{
	$st = $conn['st']["login_user${type}_q"];
	if (!isset($st))
		return mmm_err(MMM_ERR_INTERNAL, "mmm_user_info(): invalid type $type");
	else if (!$st->execute(array($value)))
		return mmm_dberr($st, "Could not query user$type $value");
	$u = $st->fetchAll();
	if (count($u) < 1) {
		return mmm_err(MMM_ERR_LOGIN, 'Invalid username or password');
	} else if (count($u) > 1) {
		// FIXME: log an internal error, break-in attempt  or something
		return mmm_err(MMM_ERR_LOGIN, 'Invalid username or password');
	}

	$u = $u[0];
	if ($type == 'name' && $u['username'] != $value) {
		// FIXME: log an internal error, break-in attempt or something
		return mmm_err(MMM_ERR_LOGIN, 'Invalid username or password');
	} else if ($type == 'id' && $u['id'] != $value) {
		// FIXME: log an internal error, break-in attempt or something
		return mmm_err(MMM_ERR_LOGIN, 'Invalid username or password');
	} else if (!preg_match('/^[$][0-9]+[$][^$]+[$]/', $u['password'])) {
		// FIXME: log an unsalted password internal error or something
		return mmm_err(MMM_ERR_LOGIN, 'Invalid username or password');
	}
	return $u;
}

function mmm_session_info(array $conn, string $sid): ?array
{
	$st = $conn['st']['login_session_q'];
	if (!$st->execute(array($sid)))
		return mmm_dberr($st, "Could not query session $sid");
	$u = $st->fetchAll();
	if (count($u) < 1) {
		return mmm_err(MMM_ERR_INVALID_SID, 'Invalid session ID');
	} else if (count($u) > 1) {
		// FIXME: log an internal error, break-in attempt  or something
		return mmm_err(MMM_ERR_INVALID_SID, 'Invalid session ID');
	}

	$u = $u[0];
	if ($u['id'] != $sid) {
		// FIXME: log an internal error, break-in attempt or something
		return mmm_err(MMM_ERR_INVALID_SID, 'Invalid session ID');
	}
	return $u;
}

function mmm_session_user_info(array $conn, string $sid): ?array
{
	$s = mmm_session_info($conn, $sid);
	if (!isset($s))
		return null;
	$u = mmm_user_info($conn, 'id', $s['user_id']);
	if (!isset($u))
		return null;

	$st = $conn['st']['login_session_update'];
	if (!$st->execute(array($sid)))
		return mmm_dberr($st, 'Could not update the session info');

	return array(
	    'sid' => $sid,
	    'username' => $u['username'],
	    'userid' => $u['id'],
	    'name' => $u['name'],
	    'admin' => $u['admin'],
	    'conn' => $conn,
	);
}

// The domain ID can be either a string or an integer.
// We can't use union types before PHP 8.0.
function mmm_check_domain_access(array $s, string $domain_type, $domain_id): bool
{
	if ($s['admin'] == 'Y')
		return true;

	$qname = "access_user_domain_$domain_type";
	if (!isset($s['conn']['st'][$qname])) {
		mmm_err(MMM_ERR_INTERNAL, "Bad domain type $domain_type");
		return false;
	}
	$st = $s['conn']['st'][$qname];
	if (!$st->execute(array($s['userid'], $domain_id))) {
		mmm_dberr($st, 'Could not get the domain information');
		return false;
	}
	$res = $st->fetchAll();
	if (count($res) != 1 || $res[0][0] != 1) {
		mmm_err(MMM_ERR_USAGE, 'Could not get the user access information');
		return false;
	}
	return true;
}

function MMM_MMMLogin(string $username, string $password): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$u = mmm_user_info($conn, 'name', $username);
		if (!isset($u))
			return null;

		// Okay, finally the real thing :)
		$cr = crypt($password, $u['password']);
		if ($cr != $u['password'])
			return mmm_err(MMM_ERR_LOGIN, 'Invalid username or password');

		$sid = hash('sha512', $u['username'].uniqid($u['password'], true));
		$st = $conn['st']['login_session_ins'];
		if (!$st->execute(array($sid, $u['id'])))
			return mmm_dberr($st, 'Could not create a session');
		return array(
		    'sid' => $sid,
		    'username' => $u['username'],
		    'name' => $u['name'],
		    'admin' => $u['admin'],
		);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_MMMLogout(string $sid): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		$st = $conn['st']['logout_session_del'];
		if (!$st->execute(array($sid)))
			return mmm_dberr($st, 'Could not remove the MaMoMa session');

		return array('username' => '');
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_MMMRestoreSession(string $sid): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		return mmm_session_user_info($conn, $sid);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_MMMVersion(): string
{
	return 'MaMoMa '.MMMVersion;
}

function MMM_MMMFeatures(): array
{
	return array(
		'mamoma' => MMMVersion,
		'features' => '1.0',

		'domains' => '1.0',
		'aliases' => '1.0',
		'users' => '1.0',

		'quota' => '1.0',

		'highlight' => '1.0',
		'highlight_quota' => '1.0',
	);
}

if (!function_exists('array_column')) {
	function array_column(array $arr, $key)
	{
		$res = array();
		foreach ($arr as $el)
			$res[] = $el[$key];
		return $res;
	}
}

function MMM_DomainsList(string $sid, ?string $highlight, ?int $highlight_threshold): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		if ($s['admin'] == 'Y') {
			if (!isset($highlight)) {
				$st = $conn['st']['dom_list_all_h_none'];
				$res = $st->execute();
			} elseif ($highlight == "quota") {
				$st = $conn['st']['dom_list_all_h_quota'];
				$res = $st->execute(array($highlight_threshold));
			} else {
				return mmm_err(MMM_ERR_INTERNAL, "How did we get here with highlight '$highlight'?");
			}
		} else {
			$st = $conn['st']['dom_list_allowed'];
			$res = $st->execute(array($s['userid']));
		}
		if (!$res)
			return mmm_dberr($st, 'Could not fetch the list of domains');
		$conf = mmm_conf();
		return array(
			'available_mb' => intval($conf['fs']['available_mb']),
			'domains' => array_map(function($data) {
				return array(
					'name' => htmlentities($data['name']),
					'description' => htmlentities($data['description']),
					'quota_mb' => intval($data['quota_mb']),
					'used_mb' => intval($data['used_mb']),
					'highlighted' => $data['highlighted'] == 'Y',
				);
			}, $st->fetchAll()),
		);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_UsersList(string $sid, string $domain): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		if (!mmm_check_domain_access($s, 'name', $domain))
			return mmm_err(MMM_ERR_USAGE, "Could not fetch the list of users");
		if ($s['admin'] == 'Y') {
			/* Do not let 'dom_list_users' return 0 records for a non-existent domain. */
			$st = $conn['st']['dom_get_by_name'];
			if (!$st->execute(array($domain)))
				return mmm_dberr($st, 'Could not get the domain information');
			$res = $st->fetchAll();
			if (count($res) == 0)
				return mmm_err(MMM_ERR_USAGE, "Could not fetch the list of users");
		}

		$st = $conn['st']['dom_list_users'];
		if (!$st->execute(array($domain)))
			return mmm_dberr($st, 'Could not fetch the list of users');
		$data = $st->fetchAll();
		foreach ($data as $_id => &$u) {
			if (!isset($u['quota_kb']))
				$u['quota_mb'] = null;
			else
				$u['quota_mb'] = floor($u['quota_kb'] / 1024);
			unset($u['quota_kb']);

			if (!isset($u['used_kb']))
				$u['used_mb'] = null;
			else
				$u['used_mb'] = floor($u['used_kb'] / 1024);
			unset($u['used_kb']);
		}
		return $data;
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_AliasesList(string $sid, string $domain): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		if (!mmm_check_domain_access($s, 'name', $domain))
			return mmm_err(MMM_ERR_USAGE, "Could not fetch the list of aliases");
		if ($s['admin'] == 'Y') {
			/* Do not let 'dom_list_aliases' return 0 records for a non-existent domain. */
			$st = $conn['st']['dom_get_by_name'];
			if (!$st->execute(array($domain)))
				return mmm_dberr($st, 'Could not get the domain information');
			$res = $st->fetchAll();
			if (count($res) == 0)
				return mmm_err(MMM_ERR_USAGE, "Could not fetch the list of aliases");
		}

		$st = $conn['st']['dom_list_aliases'];
		if (!$st->execute(array($domain)))
			return mmm_dberr($st, 'Could not fetch the list of aliases');
		$data = $st->fetchAll();
		return $data;
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function mmm_email_user_getinfo(array $conn, array $s, array $u): ?array
{
	if (!isset($u['email']))
		return mmm_err(MMM_ERR_USAGE, 'No user e-mail address specified');
	$st = $conn['st']['user_get'];
	$res = $st->execute(array($u['email']));
	if (!$res)
		return mmm_dberr($st, 'Could not fetch the requested user info');
	return $st->fetchAll();
}

function mmm_email_alias_getinfo(array $conn, array $s, array $u): ?array
{
	if (!isset($u['source']))
		return mmm_err(MMM_ERR_USAGE, 'No alias source address specified');
	if (isset($u['id'])) {
		$st = $conn['st']['alias_get'];
		$res = $st->execute(array($u['source'], $u['id']));
	} else if (isset($u['destination'])) {
		$st = $conn['st']['alias_get_by_destination'];
		$res = $st->execute(array($u['source'], $u['destination']));
	} else {
		return mmm_err(MMM_ERR_USAGE, 'Neither alias internal ID nor alias destination specified');
	}
	if (!$res)
		return mmm_dberr($st, 'Could not fetch the requested alias info');
	return $st->fetchAll();
}

function mmm_email_user_info(array $conn, array $s, array $u): ?array
{
	$current = mmm_email_user_getinfo($conn, $s, $u);
	if (!isset($current))
		return null;
	if (count($current) != 1 || $current[0]['email'] != $u['email'])
		return mmm_err(MMM_ERR_USAGE, 'Could not fetch the requested user info');
	$current = $current[0];

	if (!mmm_check_domain_access($s, 'id', $current['domain_id']))
		return mmm_err(MMM_ERR_USAGE, 'Could not fetch the requested user info');
	return $current;
}

function mmm_email_alias_info(array $conn, array $s, array $u): ?array
{
	$current = mmm_email_alias_getinfo($conn, $s, $u);
	if (!isset($current))
		return null;
	if (count($current) != 1 || $current[0]['source'] != $u['source'])
		return mmm_err(MMM_ERR_USAGE, 'Could not fetch the requested alias info');
	$current = $current[0];

	if (!mmm_check_domain_access($s, 'id', $current['domain_id']))
		return mmm_err(MMM_ERR_USAGE, 'Could not fetch the requested alias info');
	return $current;
}

function mmm_pw_crypt(string $passwd): string
{
	$salt = '$6$'.substr(md5(uniqid()), 0, 16).'$';
	return crypt($passwd, $salt);
}

function MMM_UserUpdate(string $sid, array $u): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		$u['email'] = strtolower($u['email']);
		$current = mmm_email_user_info($conn, $s, $u);
		if (!isset($current))
			return null;

		$conf = mmm_conf();
		if (isset($u['quota_mb']) && $u['quota_mb'] == '')
			$u['quota_mb'] = null;
		if (isset($u['quota_mb']))
			$u['quota_kb'] = $u['quota_mb'] * 1024;
		elseif (isset($conf['fs']['quota_kb']))
			$u['quota_kb'] = $conf['fs']['quota_kb'];
		else
			return mmm_err(MMM_ERR_USAGE,
			    'No quota specified and no default quota configured');
		if (isset($u['password']) && $u['password'] != '') {
			$st = $conn['st']['user_update_pw'];
			$encrypted = mmm_pw_crypt($u['password']);
			$res = $st->execute(array($u['active'], $u['quota_kb'], $u['home'], $u['uid'], $u['gid'], $encrypted, $current['id']));
		} else {
			$st = $conn['st']['user_update'];
			$res = $st->execute(array($u['active'], $u['quota_kb'], $u['home'], $u['uid'], $u['gid'], $current['id']));
		}
		if (!$res)
			return mmm_dberr($st, 'Could not update the user information');
		return array('email' => $current['email']);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_AliasUpdate(string $sid, array $u): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		$u['source'] = strtolower($u['source']);
		$current = mmm_email_alias_info($conn, $s,
		    array('source' => $u['source'], 'id' => $u['id']));
		if (!isset($current))
			return null;
		$duplicate = mmm_email_alias_info($conn, $s,
		    array('source' => $u['source'], 'destination' => $u['destination']));
		if (isset($duplicate) && $duplicate['id'] != $current['id'])
			return mmm_err(MMM_ERR_USAGE,
			    'An alias with the specified source and destination addresses already exists');

		$st = $conn['st']['alias_update'];
		$res = $st->execute(array($u['active'], $u['destination'], $current['id']));
		if (!$res)
			return mmm_dberr($st, 'Could not update the alias information');
		return array('source' => $current['source'], 'id' => $current['id']);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function mmm_aget(array $a, string $key): ?string
{
	return isset($a[$key])? $a[$key]: null;
}

function mmm_empty(?string $s): bool
{
	return !isset($s) || $s == '';
}

function MMM_DomainCreate(string $sid, array $d): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;
		if ($s['admin'] != 'Y')
			return mmm_err(MMM_ERR_NOT_ADMIN,
			    'Administrative access required');

		$st = $conn['st']['dom_get_by_name'];
		if (!$st->execute(array($d['name'])))
			return mmm_dberr($st,
			    'Could not get the domain information');
		$res = $st->fetchAll();
		if (count($res) != 0)
			return mmm_err(MMM_ERR_EXISTS,
			    'A domain with that name already exists');

		$st = $conn['st']['dom_insert'];
		if (!$st->execute(array($d['name'])))
			return mmm_dberr($st,
			    'Could not create the domain');
		return array('name' => $d['name']);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_UserCreate(string $sid, array $u): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		$u['email'] = strtolower($u['email']);
		$udata = explode('@', $u['email']);
		if (count($udata) != 2)
			return mmm_err(MMM_ERR_USAGE,
			    'No e-mail domain specified');
		if (!preg_match('/^[A-Za-z0-9_.-]+$/', $udata[0]))
			return mmm_err(MMM_ERR_USAGE,
			    'Invalid e-mail username specified');
		$st = $conn['st']['dom_get_by_name'];
		if (!$st->execute(array($udata[1])))
			return mmm_dberr($st,
			    'Could not get the domain information');
		$res = $st->fetchAll();
		if (count($res) != 1)
			return mmm_err(MMM_ERR_USAGE,
			    'Could not get the domain information');
		$domain_id = $res[0]['id'];
		if (!mmm_check_domain_access($s, 'id', $domain_id))
			return mmm_err(MMM_ERR_USAGE,
			    'Could not get the domain information');

		$current = mmm_email_user_getinfo($conn, $s, $u);
		if (isset($current) && count($current) != 0)
			return mmm_err(MMM_ERR_EXISTS,
			    'User account already exists');

		$conf = mmm_conf();
		if (isset($u['quota_mb']) && $u['quota_mb'] == '')
			$u['quota_mb'] = null;
		if (isset($u['quota_mb']))
			$u['quota_kb'] = $u['quota_mb'] * 1024;
		elseif (isset($conf['fs']['quota_kb']))
			$u['quota_kb'] = $conf['fs']['quota_kb'];
		else
			return mmm_err(MMM_ERR_USAGE,
			    'No quota specified and no default quota configured');
		$encrypted = mmm_pw_crypt($u['password']);

		if (mmm_empty($u['home']))
			$u['home'] = sprintf('%s/%s/%s',
			    $conf['fs']['basedir'], $udata[1], $udata[0]);
		if (mmm_empty($u['uid']))
			$u['uid'] = $conf['fs']['uid'];
		if (mmm_empty($u['gid']))
			$u['gid'] = $conf['fs']['gid'];

		$st = $conn['st']['user_insert'];
		$res = $st->execute(array($domain_id, $u['email'],
		    $u['active'], $u['quota_kb'], $u['home'],
		    $u['uid'], $u['gid'], $encrypted));
		if (!$res)
			return mmm_dberr($st,
			    'Could not update the user information');
		return array('email' => $u['email']);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_AliasCreate(string $sid, array $u): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		$u['source'] = strtolower($u['source']);
		$udata = explode('@', $u['source']);
		if (count($udata) != 2)
			return mmm_err(MMM_ERR_USAGE,
			    'No e-mail domain specified');
		if (!preg_match('/^[A-Za-z0-9_.-]+$/', $udata[0]))
			return mmm_err(MMM_ERR_USAGE,
			    'Invalid e-mail alias specified');
		$st = $conn['st']['dom_get_by_name'];
		if (!$st->execute(array($udata[1])))
			return mmm_dberr($st,
			    'Could not get the domain information');
		$res = $st->fetchAll();
		if (count($res) != 1)
			return mmm_err(MMM_ERR_USAGE,
			    'Could not get the domain information');
		$domain_id = $res[0]['id'];
		if (!mmm_check_domain_access($s, 'id', $domain_id))
			return mmm_err(MMM_ERR_USAGE,
			    'Could not get the domain information');

		if (isset($u['id']))
			return mmm_err(MMM_ERR_USAGE,
			    'Cannot specify an internal ID when creating an alias');
		else if (!isset($u['destination']))
			return mmm_err(MMM_ERR_USAGE,
			    'No alias destination specified');
		$current = mmm_email_alias_getinfo($conn, $s, $u);
		if (isset($current) && count($current) != 0)
			return mmm_err(MMM_ERR_EXISTS,
			    'Alias already exists');

		$st = $conn['st']['alias_insert'];
		$res = $st->execute(array($domain_id, $u['source'],
		    $u['active'], $u['destination']));
		if (!$res)
			return mmm_dberr($st,
			    'Could not update the alias information');
		/* OK, we need to get the ID back */
		return mmm_email_alias_getinfo($conn, $s, $u);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_UserDelete(string $sid, ?string $email): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		$current = mmm_email_user_info($conn, $s,
		    array('email' => $email));
		if (!isset($current))
			return null;

		$st = $conn['st']['user_delete'];
		if (!$st->execute(array($current['id'])))
			return mmm_dberr($st, 'Could not delete the user account');
		return array('email' => $current['email']);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}

function MMM_AliasDelete(string $sid, array $u): ?array
{
	try {
		$conn = mmm_dbconn();
		if (!isset($conn))
			return null;

		$s = mmm_session_user_info($conn, $sid);
		if (!isset($s))
			return null;

		$current = mmm_email_alias_info($conn, $s,
		    array('source' => $u['source'], 'id' => $u['id']));
		if (!isset($current))
			return null;

		$st = $conn['st']['alias_delete'];
		if (!$st->execute(array($current['id'])))
			return mmm_dberr($st, 'Could not delete the alias');
		return array('source' => $current['source'], 'id' => $current['id']);
	} catch (PDOException $e) {
		return mmm_err(MMM_ERR_DB, $e->getMessage());
	}
}
?>
