#!/usr/bin/perl

use v5.010;
use strict;
use warnings;

use DBI;

sub get_db_config($)
{
	my ($fname) = @_;

	open my $f, '<', $fname or
	    die "Could not open $fname: $!\n";
	my $word = qr/[A-Za-z0-9_]+/;
	my %data;
	my $in_db;
	while (<$f>) {
		chomp;
		if ($in_db) {
			if (/^\s*\[\s*(?<section>$word)\s*\]\s*/) {
				$in_db = $+{section} eq 'db';
				next;
			} elsif (!/^\s*(?<var>$word)\s*=\s*'(?<val>[^']*)'\s*$/) {
				next;
			}
			my ($var, $val) = ($+{var}, $+{val});
			if ($var ne 'db') {
				$data{$var} = $val;
				next;
			} elsif ($val !~ /^(?<driver>$word):(?<vars>.*)/) {
				die "Invalid db.db line in $fname\n";
			}
			$data{driver} = $+{driver};
			foreach my $pair (split /;/, $+{vars}) {
				if ($pair !~ /^(?<var>$word)=(?<val>.*)$/) {
					die "Invalid pair '$pair' in the db.db line in $fname\n";
				}
				$data{$+{var}} = $+{val};
			}
		} elsif (/^\s*\[\s*db\s*\]\s*$/) {
			$in_db = 1;
		}
	}
	close $f or die "Could not close $fname after reading: $!\n";
	if (!defined $data{driver}) {
		die "No db.db line in $fname\n";
	}

	my %xlat = (
		mysql => {
			dbname => 'database',
			host => 'host',
			password => 'password',
			port => 'port',
			username => 'username',
			_dsn => [ qw(database host port) ],
		},
	);
	my $x = $xlat{$data{driver}};
	if (!defined $x) {
		die "Unsupported database driver $data{driver}\n";
	}
	my %new = map {
		defined $data{$_}? ($x->{$_} => $data{$_}): ()
	} keys %{$x};
	my $dsn = "dbi:$data{driver}:".
	    join ';', map "$_=$new{$_}", grep defined $new{$_}, @{$x->{_dsn}};
	return {
		dsn => $dsn,
		username => $new{username},
		password => $new{password},
	};
}

sub process_user($ $)
{
	my ($user, $quota) = @_;
	my ($type, $username, $value) = @{$user}{qw(Type Username Value)};
	if (!defined $type) {
		use Data::Dumper;
		die "doveadm returned a user record without a type: ".Dumper($user);
	} elsif ($type ne 'STORAGE') {
		return;
	} elsif (!defined $username || !defined $value) {
		die "doveadm returned a user record with no username or value\n";
	} elsif ($value !~ /^(0|[1-9][0-9]*)$/) {
		die "doveadm returned a user record with a non-positive-integer value\n";
	}
	$quota->{$username} = $value;
}

MAIN:
{
	die "Usage: mamoma-dovecot-used-quota <conffile>\n"
	    unless @ARGV == 1;
	my $conf = get_db_config $ARGV[0];
	my $dbh = DBI->connect($conf->{dsn}, $conf->{username}, $conf->{password},
	    { RaiseError => 1 });

	open my $f, '-|', 'doveadm', '-f', 'pager', 'quota', 'get', '-A' or
	    die "Could not fork for doveadm: $!\n";
	my (%user, %quota);
	while (<$f>) {
		chomp;
		if ($_ eq "\f") {
			process_user \%user, \%quota;
			undef %user;
			next;
		} elsif (/^(?<var>[A-Za-z0-9 %]+):\s*(?<val>.*)$/) {
			$user{$+{var}} = $+{val};
		} elsif (/^(?<val>(?:\S+)[@](?:\S+))$/) {
			if (%user) {
				die "doveadm returned an unexpected e-mail-address-like line when a user account definition has already started: $_\n";
			} else {
				$user{Username} = $+{val};
			}
		} else {
			die "doveadm returned an unexpected line: $_\n";
		}
	}
	close $f or die "Could not close the pipe to doveadm: $!\n";
	my $dstat = $?;
	if ($dstat & 127) {
		die "doveadm was killed by signal ".($dstat & 127)."\n";
	} elsif ($dstat != 0) {
		die "doveadm returned a non-zero exit status ".($dstat >> 8)."\n";
	}
	process_user \%user, \%quota if %user;

	my $sth = $dbh->prepare('UPDATE virtual_users SET used_kb = ? WHERE email = ?');
	$dbh->do('BEGIN');
	$dbh->do('UPDATE virtual_users SET used_kb = NULL');
	for my $email (keys %quota) {
		$sth->execute($quota{$email}, $email);
	}
	$dbh->do('COMMIT');
	$dbh->disconnect;
}
