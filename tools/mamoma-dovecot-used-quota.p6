#!/usr/bin/env perl6
#
# Copyright (c) 2016  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v6.c;

use DBIish;

class CommandOutput
{
	has Int:D $.exitcode is required;
	has Str:D $.output is required;
}

sub run-capture(*@cmd) returns CommandOutput:D
{
	my Proc:D $p = run @cmd, :out;
	my Str:D $out = $p.out.slurp-rest;
	my $exit = $p.out.close;
	return CommandOutput.new(:exitcode($p.exitcode), :output($out));
}

sub note-fatal(Str:D $s)
{
	note $s;
	exit 1;
}

grammar PHP-INI {
	token TOP { \s* <section>* }
	token section { <db-section> | <random-line> }

	token db-section { <db-header> <db-line>* }
	token db-header { '[' <[\ \t]>* 'db' <[\ \t]>* ']' <[\ \t]>* \n \s* }
	token db-line { <db-line-db> | <db-line-kv> }

	regex db-line-db { 'db' <ws> '=' <ws> <[']> <db-driver> ':' <db-pairs> <[']> <ws> \n \s* }
	token db-driver { <word> }
	regex db-pairs { <db-pair-single> | [ <db-pair> ';' <db-pairs> ] }
	token db-pair-single { <db-pair> }
	token db-pair { <db-pair-key> '=' <db-pair-value> }
	token db-pair-key { <word> }
	token db-pair-value { <-[;'\n]>* }

	token db-line-kv { <key> <ws> '=' <ws> <value> <ws> \n \s* }
	token key { <word> }
	token value { [ <value-str> || <value-uint> ] }
	token value-str { <[']> <value-str-contents> <[']> }
	token value-str-contents { <-['\n]>* }
	token value-uint { [ '0' | <[1..9]> <[0..9]>* ] }
	token word { <[A..Za..z0..9_]>+ }

	token random-line { <-[\n]>* \n }

	token ws { <[\ \t]>* }

	my %db-xlat =
		mysql => {
			:dbname('database'),
			:username('user'),
		};

	class Actions {
		method TOP($/) {
			my Str:D %data = Hash(flat |$<section>».made);
			my Str $driver = %data<driver>:delete;
			note-fatal "No db.db setting in the config file" unless $driver.defined;
			note-fatal "Unrecognized database driver $driver"
			    unless %db-xlat{$driver}:exists;
			my Str:D %res;
			for %data.keys -> Str:D $k {
				%res{%db-xlat{$driver}{$k} // $k} = %data{$k};
			}
			make { driver => $driver, opts => %res };
		}
		method section($/) { make $<db-section>?? $<db-section>.made!! () }

		method db-section($/) { make flat |$<db-line>».made }
		method db-line($/) { make $<db-line-db>?? $<db-line-db>.made!! $<db-line-kv>.made }

		method db-line-db($/) { make (driver => ~$<db-driver>, $<db-pairs>.made).flat }
		method db-pairs($/) { make $<db-pair-single>?? $<db-pair-single><db-pair>.made!! ($<db-pair>.made, $<db-pairs>.made).flat }
		method db-pair($/) { make ~$<db-pair-key> => ~$<db-pair-value> }

		method db-line-kv($/) { make (~$<key> => $<value>.made) }
		method value($/) { make $<value-str>?? ~$<value-str><value-str-contents>!! +$<value-uint> }
	}

	method parse(|c) { nextwith(actions => Actions, |c) }
	method subparse(|c) { nextwith(actions => Actions, |c) }
}

grammar DoveAdm {
	token TOP { <user>* }
	token user { <u-line>+<ff-line> }

	token u-line { <field> ':' \s* <value> \n }
	token field { <[A..Za..z0..9\ %]>+ }
	token value { <-[\n]>* }

	token ff-line { \f \n }

	class Actions {
		method TOP($/) { make Hash(flat $<user>».made) }
		method user($/) {
			my %data = Hash($<u-line>».made);
			note-fatal "doveadm returned an incomplete record:\n$/"
			    unless all map { %data{$_}:exists }, <Username Type Value>;
			make %data<Type> ne 'STORAGE'?? ()!!
			    %data<Username> => +%data<Value>;
		}
		method u-line($/) { make ~$<field> => ~$<value> }
	}

	method parse(|c) { nextwith(actions => Actions, |c) }
	method subparse(|c) { nextwith(actions => Actions, |c) }
}

sub MAIN(Str:D $conffile)
{
	my $conf-contents = $conffile.IO.slurp;
	my $parsed = PHP-INI.parse($conf-contents) or
	    note-fatal "Could not parse the config file $conffile";
	my %conf = $parsed.made;
	my $dbh = DBIish.connect(%conf<driver>, |%conf<opts>);

	my CommandOutput:D $res = run-capture <doveadm quota get -A -f pager>;
	note-fatal "doveadm died with exit code $res.exitcode()"
	    if $res.exitcode != 0;
	$parsed = DoveAdm.parse($res.output) or
	    note-fatal 'Could not parse the doveadm output';
	my %quota = $parsed.made;

	my $sth = $dbh.prepare('UPDATE virtual_users SET used_kb = ? WHERE email = ?');

	$dbh.do('BEGIN');
	$dbh.do('UPDATE virtual_users SET used_kb = NULL');
	for %quota.kv -> Str:D $email, UInt:D $quota {
		$sth.execute($quota, $email);
	}
	$dbh.do('COMMIT');
}
