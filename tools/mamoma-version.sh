#!/bin/sh
#
# Copyright (c) 2014  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

unset newsfile
for f in NEWS ../NEWS mamoma/NEWS; do
	if [ -f "$f" ]; then
		newsfile="$f"
		break
	fi
done
if [ -z "$newsfile" ]; then
	echo "Could not determine the MaMoMa top directory, no NEWS file anywhere around" 1>&2
	exit 1
fi

fline=`egrep -e '^[0-9]+\.[0-9]+[[:space:]]' "$newsfile" | head -n1`
set -- $fline
ver=$1
shift
if [ "$#" = 1 ] && expr "x$1" : 'x[0-9]\+-[0-9]\+-[0-9]\+$' > /dev/null; then
	reldate="$1"
	relstring='release'
else
	if [ -n "`git status --short --untracked-files=no`" ]; then
		reldate="`date '+%Y-%m-%d'`-dirty"
	else
		reldate="`git log -n1 --format='%ad' --date=short`"
	fi
	relstring="work-$reldate"
fi

echo "mamoma-$ver-$relstring"
