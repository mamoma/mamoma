<?php
/*-
 * Copyright (c) 2014  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

require('../lib/mmm-core.php');

print MMM_MMMVersion()."\n";

$sid = 'bb300fc0af0dc3b0b9140b07c7e8395a272935738a0aff37de2625475a73a3a3b7c8faf91c109ff6126912575fdb58460bbe7600df2ae89faa4f7f58c7875b76';
$res = MMM_MMMRestoreSession($sid);
if (!isset($res)) {
	if ($mmm_err_code != MMM_ERR_LOGIN)
		die("MMMRestoreSession error: $mmm_err_code: $mmm_err\n");
	$res = MMM_MMMLogin('roam', 'foo');
	if (!isset($res))
		die("MMMLogin error: $mmm_err_code: $mmm_err\n");
}
print "MMM_Login() or MMMRestoreSession() returned something:\n";
print_r($res);

$res = MMM_DomainsList($sid, null, null);
if (!isset($res))
	die("DomainsList error: $mmm_err_code: $mmm_err\n");
print "Got some domains:\n";
print_r($res);
?>
